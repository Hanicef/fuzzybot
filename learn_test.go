package main

import (
	"testing"
)

func compareEscape(context *testing.T, in, expect string) {
	text := escapeHTML([]byte(in))
	if text != expect {
		if expect != "" {
			context.Error(text, "!=", expect)
		} else {
			context.Error(in, "is not empty")
		}
	}
}

func TestEscape(context *testing.T) {
	compareEscape(context, "Hello, &amp;world!", "Hello, &world!")
	compareEscape(context, "&quot;quote&quot;", "\"quote\"")
	compareEscape(context, "emp&#x21;", "emp!")
	compareEscape(context, "emp&#33;", "emp!")
	compareEscape(context, "&invalid", "")
	compareEscape(context, "&;", "")
	compareEscape(context, "&inv;", "")
	compareEscape(context, "&#;", "")
	compareEscape(context, "&#x;", "")
}

