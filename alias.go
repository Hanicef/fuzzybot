
package main

import (
	"gitlab.com/Hanicef/fuzzybot/engine"
	"log"
	"fmt"
	"strconv"
	"database/sql"
	"errors"
	"strings"
	"regexp"
)

func InitAlias() {
	_, err := engine.Database.Exec("CREATE TABLE IF NOT EXISTS Alias(command STRING NOT NULL PRIMARY KEY, expand STRING NOT NULL, owner INTEGER REFERENCES Nick(id))")
	if err != nil {
		log.Fatal("ERROR:", err.Error())
	}
}

func expand(alias string, nick string, args []string, validate bool) (string, error) {
	var out []byte = make([]byte, 0, len(alias))
	for i := 0; i < len(alias); i++ {
		if alias[i] == '%' {
			if i+1 == len(alias) {
				return "", errors.New("unfinished sequence at EOL")
			} else {
				switch alias[i+1] {
				case '%':
					out = append(out, '%')

				case 'u':
					out = append(out, []byte(nick)...)

				case '0','1','2','3','4','5','6','7','8','9':
					argn := alias[i+1] - '0'
					if args != nil && int(argn) < len(args) {
						out = append(out, []byte(args[argn])...)
					}

				case '*':
					if args != nil {
						out = append(out, []byte(strings.Join(args, " "))...)
					}

				case 'i':
					var j, t, e, d int = 0, -1, -1, 0
					for j = i + 2; j < len(alias); j++ {
						if alias[j] == '%' {
							switch alias[j+1] {
							case 'i':
								// Allow nesting aliases.
								d++

							case 't':
								if d == 0 {
									t = j
								}

							case 'e':
								if d == 0 {
									e = j
								}

							case 'x':
								d--
							}
						}

						if d < 0 {
							break
						}
					}

					if j == len(alias) {
						return "", errors.New("missing %x in conditional statement")
					} else if t == -1 {
						return "", errors.New("missing %t in conditional statement")
					}

					// Do this without expanding to prevent alias injection.
					equal := strings.Index(alias[i+2:t], "=")
					if equal == -1 {
						return "", errors.New("condition has no comparator character")
					}

					result := equal > 0 && (alias[i+2+equal-1] == '/' || alias[i+2+equal-1] == '!')

					condition, err := expand(alias[i+2:t], nick, args, validate)
					if err != nil {
						return "", err
					}
					equal = strings.Index(condition, "=")
					if equal == -1 {
						return "", errors.New("missing = in conditional statement")
					}

					var operands [2]string
					if !result {
						operands[0] = condition[:equal]
					} else {
						operands[0] = condition[:equal-1]
					}
					operands[1] = condition[equal+1:]

					if operands[0] == operands[1] {
						result = !result
					}

					if validate {
						// On validation, validate both cases regardless of the result.
						if e != -1 {
							_, err = expand(alias[t+2:e], nick, args, validate)
							if err != nil {
								return "", err
							}

							_, err = expand(alias[e+2:j], nick, args, validate)
							if err != nil {
								return "", err
							}
						} else {
							_, err = expand(alias[t+2:j], nick, args, validate)
							if err != nil {
								return "", err
							}
						}
					} else {
						if result {
							if e != -1 {
								result, err := expand(alias[t+2:e], nick, args, validate)
								if err != nil {
									return "", err
								}

								out = append(out, []byte(result)...)
							} else {
								result, err := expand(alias[t+2:j], nick, args, validate)
								if err != nil {
									return "", err
								}

								out = append(out, []byte(result)...)
							}
						} else {
							if e != -1 {
								result, err := expand(alias[e+2:j], nick, args, validate)
								if err != nil {
									return "", err
								}

								out = append(out, []byte(result)...)
							}
						}
					}

					i = j

				case '{':
					var d, j = 0, 0
					for j = i+2; j < len(alias); j++ {
						if j < len(alias)-1 && alias[j] == '%' && alias[j+1] == '{' {
							// Allow nesting.
							d++
						} else if alias[j] == '}' {
							if d == 0 {
								break
							} else {
								d--
							}
						}
					}
					if j == len(alias) {
						return "", errors.New("command missing corresponding closing bracket")
					}

					command, err := expand(alias[i+2:j], nick, args, validate)
					if err != nil {
						return "", err
					}

					split := strings.SplitN(command, " ", 2)

					// Create a fake identifier for this case.
					invoker := engine.Identifier{}
					invoker.Nick = &engine.Nickname{nick, engine.ModeNone, false}
					invoker.Command = split[0]
					if len(split) > 1 {
						invoker.Parameter = &split[1]
					} else {
						invoker.Parameter = nil
					}
					method, ok := engine.Methods[split[0]]
					if !ok {
						return "", fmt.Errorf("method %s doensn't exist", split[0])
					}

					// Unlock the database first, in case the method uses the database.
					engine.UnlockDatabase()
					out = append(out, []byte(method.Func(&invoker))...)
					engine.LockDatabase()
					i = j

				case 't','e','x':
					return "", fmt.Errorf("sequence %%%c outside of conditional statement", alias[i+1])
				default:
					return "", fmt.Errorf("unknown sequence %%%c", alias[i+1])
				}

				i++
			}
		} else {
			out = append(out, alias[i])
		}
	}

	return string(out), nil
}

func AddAlias(source *engine.Identifier) string {
	if !source.Nick.Present {
		return "This command cannot be used when absent from channel"
	}

	if source.Parameter == nil {
		return "Missing name parameter"
	}

	split := strings.SplitN(*source.Parameter, " ", 2)
	if len(split) != 2 {
		return "Missing expansion parameter"
	}

	engine.LockDatabase()
	defer engine.UnlockDatabase()

	nick, err := engine.RegisterNick(source.Nick.Nick)
	if err != nil {
		return fmt.Sprint("Failed to register nickname: ", err.Error())
	}

	// We need to expand to check for errors.
	_, err = expand(split[1], source.Nick.Nick, nil, true)
	if err != nil {
		return fmt.Sprint("Syntax error: ", err.Error())
	}

	_, err = engine.Database.Exec("INSERT INTO Alias(command, expand, owner) VALUES ($1, $2, $3)", split[0], split[1], nick)
	if err != nil {
		return fmt.Sprint("Failed to save alias: ", err.Error())
	}

	return "Done"
}

func EditAlias(source *engine.Identifier) string {
	var owner int64

	if !source.Nick.Present {
		return "This command cannot be used when absent from channel"
	}

	if source.Parameter == nil {
		return "Missing name parameter"
	}

	split := strings.SplitN(*source.Parameter, " ", 2)
	if len(split) != 2 {
		return "Missing expansion parameter"
	}

	engine.LockDatabase()
	defer engine.UnlockDatabase()

	nick, err := engine.GetNick(source.Nick.Nick)
	if err != nil {
		if err == sql.ErrNoRows {
			return fmt.Sprint("Nickname ", source.Nick.Nick, " not registered")
		} else {
			return fmt.Sprint("Failed to fetch nickname: ", err.Error())
		}
	}

	row := engine.Database.QueryRow("SELECT owner FROM Alias WHERE command = $1", split[0])
	err = row.Scan(&owner)
	if err != nil {
		if err == sql.ErrNoRows {
			return fmt.Sprint("Alias ", split[0], " doesn't exist")
		} else {
			return fmt.Sprint("Failed to query alias owner: ", err.Error())
		}
	}

	if owner != nick {
		return fmt.Sprint("Insufficient permissions to edit ", split[0])
	}

	// We need to expand to check for errors.
	_, err = expand(split[1], source.Nick.Nick, nil, true)
	if err != nil {
		return fmt.Sprint("Syntax error: ", err.Error())
	}

	_, err = engine.Database.Exec("UPDATE Alias SET expand = $1 WHERE command = $2", split[1], split[0])
	if err != nil {
		return fmt.Sprint("Failed to update alias: ", err.Error())
	}

	return "Done"
}

func DelAlias(source *engine.Identifier) string {
	var owner int64

	if !source.Nick.Present {
		return "This command cannot be used when absent from channel"
	}

	if source.Parameter == nil {
		return "Missing name parameter"
	}

	var command string
	if (*source.Parameter)[0] == '!' {
		command = (*source.Parameter)[1:]
	} else {
		command = *source.Parameter
	}

	engine.LockDatabase()
	defer engine.UnlockDatabase()

	nick, err := engine.GetNick(source.Nick.Nick)
	if err != nil {
		if err == sql.ErrNoRows {
			return fmt.Sprint("Nickname", source.Nick.Nick, "not registered")
		} else {
			return fmt.Sprint("Failed to fetch nickname: ", err.Error())
		}
	}

	row := engine.Database.QueryRow("SELECT owner FROM Alias WHERE command = $1", command)
	err = row.Scan(&owner)
	if err != nil {
		if err == sql.ErrNoRows {
			return fmt.Sprint("Alias", command, "doesn't exist")
		} else {
			return fmt.Sprint("Failed to query alias owner: ", err.Error())
		}
	}

	if owner != nick && (source.Nick.Mode < engine.ModeOp || (*source.Parameter)[0] != '!') {
		if source.Nick.Mode >= engine.ModeOp {
			return fmt.Sprint("Insufficient permissions to remove", command, "(prepend ! to override)")
		} else {
			return fmt.Sprint("Insufficient permissions to remove", command)
		}
	}

	_, err = engine.Database.Exec("DELETE FROM Alias WHERE command = $1", command)
	if err != nil {
		return fmt.Sprint("Failed to delete alias: ", err.Error())
	}
	return "Done"
}

func ListAliases(source *engine.Identifier) string {
	var query string
	var name string

	if source.Parameter == nil {
		query = "%"
	} else {
		query = fmt.Sprintf("%%%s%%", *source.Parameter)
	}

	engine.LockDatabase()
	defer engine.UnlockDatabase()

	rows, err := engine.Database.Query("SELECT command FROM Alias WHERE command LIKE $1", query)
	if err != nil {
		return fmt.Sprint("Failed to list aliases: ", err.Error())
	}

	var out []byte = []byte("Matching alias(es) are ")
	i := 0
	for ; i < 20 && rows.Next(); i++ {
		err = rows.Scan(&name)
		if err != nil {
			return fmt.Sprint("Failed to list aliases: ", err.Error())
		}

		out = append(out, name...)
		out = append(out, ',', ' ')
	}

	out = out[:len(out)-2]
	if i < 20 {
		err = rows.Err()
		if err != nil {
			return fmt.Sprint("Failed to list aliases: ", err.Error())
		}

		if i == 0 {
			if source.Parameter == nil {
				return "No aliases exist"
			} else {
				return fmt.Sprint("No chains containing", *source.Parameter)
			}
		}
	} else {
		err = rows.Close()
		if err != nil {
			return fmt.Sprint("Failed to list chains: ", err.Error())
		}

		out = append(out, []byte("... (")...)
		out = append(out, []byte(strconv.Itoa(i))...)
		out = append(out, []byte(" more)")...)
	}
	return string(out)
}

func DefineAlias(source *engine.Identifier) string {
	var expand, owner string

	if source.Parameter == nil {
		return "No parameter specified"
	}

	engine.LockDatabase()
	defer engine.UnlockDatabase()

	row := engine.Database.QueryRow("SELECT expand, nick FROM Alias JOIN Nick ON owner = id WHERE command = $1", *source.Parameter)
	err := row.Scan(&expand, &owner)
	if err != nil {
		return fmt.Sprint("Failed to query alias: ", err.Error())
	}

	return fmt.Sprintf("%s (%s) = %s", *source.Parameter, owner, expand)
}

var AliasRegex = regexp.MustCompile(`^!(\S+)(?:\s+(.*))?`)
func OnAlias(source *engine.Identifier, matches []string) string {
	var command, alias string
	var args []string

	engine.LockDatabase()
	defer engine.UnlockDatabase()

	row := engine.Database.QueryRow("SELECT command, expand FROM Alias WHERE command = $1", matches[1])
	err := row.Scan(&command, &alias)
	if err != nil {
		if err == sql.ErrNoRows {
			return fmt.Sprint("Alias ", matches[1], " doesn't exist")
		} else {
			return fmt.Sprint("Failed to query alias: ", err.Error())
		}
	}

	if len(matches) == 2 {
		args = nil
	} else {
		args = strings.Split(matches[2], " ")
	}

	out, _ := expand(alias, source.Nick.Nick, args, false)

	if len(out) >= 4 && out[:4] == "/me " {
		return fmt.Sprintf("\001ACTION %s\001", out[4:])
	} else {
		return out
	}
}
