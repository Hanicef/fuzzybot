package main

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/Hanicef/fuzzybot/engine"
	"testing"
	"time"
)

// Feel free to replace this URL with any URL you like. The more text that the
// bot can learn from, the longer it will take to perform the benchmark.
var LearningURL = "https://golang.org/pkg/testing/"

func BenchmarkMarkov(context *testing.B) {
	var err error
	var source engine.Identifier

	engine.Database, err = sql.Open("sqlite3", ":memory:")
	if err != nil {
		context.Error("Failed to open in-memory database:", err.Error())
		return
	}

	// Databases can still fail despite opening successfully. Ping the database to make sure it's OK.
	err = engine.Database.Ping()
	if err != nil {
		context.Error("Failed to ping database:", err.Error())
		return
	}

	InitMarkov()

	source.Parameter = &LearningURL
	status := LearnHTTP(&source)
	if status != "Done" {
		context.Error("Failed to learn:", status)
		return
	}

	context.ResetTimer()
	for i := 0; i < context.N; i++ {
		generate(nil, time.Now().Unix())
	}
}

