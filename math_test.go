package main

import (
	"testing"
	"math"
)

func assertEvaluate(context *testing.T, eval string, expect float64) {
	section, err := parseMath(eval)
	if err != nil {
		context.Error(eval, "- Parsing failed:", err.Error())
		return
	}

	result, err := evaluate(section)
	if err != nil {
		context.Error(eval, "- Evaluation failed:", err.Error())
		return
	}

	if result != expect {
		context.Error(eval, "-", result, "!=", expect)
	}
}

func assertEvaluateError(context *testing.T, eval string) {
	section, err := parseMath(eval)
	if err != nil {
		return
	}

	_, err = evaluate(section)
	if err != nil {
		return
	}

	context.Error(eval, "- No error when expecting error")
}

func TestEvaluate(context *testing.T) {
	assertEvaluate(context, "3 + 4", 7.0)
	assertEvaluate(context, "3+4", 7.0)
	assertEvaluate(context, "+4 + -2", 2.0)
	assertEvaluate(context, "0.25 + 0.25", 0.5)
	assertEvaluate(context, "2 ** 4", 16.0)
	assertEvaluate(context, "pi", math.Pi)
	assertEvaluate(context, "sqrt(4)", 2.0)
	assertEvaluate(context, "(2 + 2) * 4", 16.0)
	assertEvaluate(context, "(2 + 2) + 4", 8.0)
	assertEvaluateError(context, "2 +")
	assertEvaluateError(context, "() + 4")
	assertEvaluateError(context, "euler")
	assertEvaluateError(context, "aeiou(4)")
	assertEvaluateError(context, "log2()")
	assertEvaluateError(context, "()")
	assertEvaluateError(context, "(")
	assertEvaluateError(context, " ")
	assertEvaluateError(context, "")
	assertEvaluateError(context, "!")
}
