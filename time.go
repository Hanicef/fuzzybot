
package main

import (
	"gitlab.com/Hanicef/fuzzybot/engine"
	"time"
	"log"
	"fmt"
	"database/sql"
)

func InitTime() {
	_, err := engine.Database.Exec("CREATE TABLE IF NOT EXISTS UserTimezone(user INTEGER NOT NULL PRIMARY KEY REFERENCES Nick(id), tz STRING NOT NULL)")
	if err != nil {
		log.Fatal("ERROR: ", err.Error())
	}
}

func GetTimezone(source *engine.Identifier) string {
	var tz string

	if !source.Nick.Present {
		return "This command cannot be used when absent from channel"
	}

	engine.LockDatabase()
	defer engine.UnlockDatabase()
	id, err := engine.GetNick(source.Nick.Nick)
	if err != nil {
		return "No timezone is set."
	}

	row := engine.Database.QueryRow("SELECT tz FROM UserTimezone WHERE user = $1", id)
	err = row.Scan(&tz)
	if err != nil {
		if err == sql.ErrNoRows {
			return "No timezone is set."
		} else {
			return fmt.Sprint("Couldn't query user timezone: ", err.Error())
		}
	}

	return fmt.Sprintf("%s: Your timezone is %s", source.Nick.Nick, tz)
}

func SetTimezone(source *engine.Identifier) string {
	var tz string

	if !source.Nick.Present {
		return "This command cannot be used when absent from channel"
	}

	engine.LockDatabase()
	defer engine.UnlockDatabase()
	id, err := engine.RegisterNick(source.Nick.Nick)
	if err != nil {
		return fmt.Sprint("Couldn't register nick: ", err.Error())
	}

	if source.Parameter == nil {
		_, err = engine.Database.Exec("DELETE FROM UserTimezone WHERE user = $1", id)
		if err != nil {
			return fmt.Sprint("Couldn't delete user timezone: ", err.Error())
		}
	} else {
		row := engine.Database.QueryRow("SELECT tz FROM UserTimezone WHERE user = $1", id)
		err = row.Scan(&tz)
		if err != nil {
			if err == sql.ErrNoRows {
				_, err = engine.Database.Exec("INSERT INTO UserTimezone(user, tz) VALUES ($1, $2)", id, *source.Parameter)
				if err != nil {
					return fmt.Sprint("Couldn't create user timezone: ", err.Error())
				}
			} else {
				return fmt.Sprint("Couldn't query user timezone: ", err.Error())
			}
		}

		_, err = engine.Database.Exec("UPDATE UserTimezone SET tz = $1 WHERE user = $2", *source.Parameter, id)
		if err != nil {
			return fmt.Sprint("Couldn't update user timezone: ", err.Error())
		}
	}

	return "Done"
}

func GetTime(source *engine.Identifier) string {
	var now time.Time

	if source.Parameter == nil {
		if source.Nick.Present {
			engine.LockDatabase()
			defer engine.UnlockDatabase()

			id, err := engine.GetNick(source.Nick.Nick)
			if err == nil {
				row := engine.Database.QueryRow("SELECT tz FROM UserTimezone WHERE user = $1", id)
				var tz string
				err = row.Scan(&tz)
				if err != nil {
					now = time.Now().UTC()
				} else {
					location, err := time.LoadLocation(tz)
					if err != nil {
						return fmt.Sprint("Unknown time zone ", tz)
					}

					now = time.Now().In(location)
				}
			} else {
				now = time.Now().UTC()
			}
		} else {
			now = time.Now().UTC()
		}
	} else {
		location, err := time.LoadLocation(*source.Parameter)
		if err != nil {
			return fmt.Sprint("Unknown time zone ", *source.Parameter)
		}

		now = time.Now().In(location)
	}

	return now.Format("2006-01-02 15:04:05 MST")
}

