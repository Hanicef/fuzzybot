
package main

import (
	"testing"
	"gitlab.com/Hanicef/fuzzybot/engine"
)

var chooseParam = "foo bar baz"

func assertUtil(context *testing.T, f func(*engine.Identifier) string, param, expect string) {
	var identifier engine.Identifier
	identifier.Parameter = &param
	out := f(&identifier)
	if out != expect {
		context.Error(param, "-", out, "!=", expect)
	}
}

func TestUtilMethods(context *testing.T) {
	var identifier engine.Identifier
	identifier.Parameter = &chooseParam
	choice := Choose(&identifier)
	if choice != "foo" && choice != "bar" && choice != "baz" {
		context.Error("Invalid choice", choice)
	}

	assertUtil(context, Whisper, "sample text", "ˢᵃᵐᵖˡᵉ ᵗᵉˣᵗ")
	assertUtil(context, Scream, "sample text", "ｓａｍｐｌｅ  ｔｅｘｔ")
	assertUtil(context, Nice, "sample text", "𝓼𝓪𝓶𝓹𝓵𝓮 𝓽𝓮𝔁𝓽")
	assertUtil(context, Caesar, "4 sample text", "weqtpi xibx")
}
