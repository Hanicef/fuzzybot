
package main

import (
	"gitlab.com/Hanicef/fuzzybot/engine"
	"math/rand"
	"time"
)

func main() {
	// Initiate randomizer first.
	rand.Seed(time.Now().Unix())

	sections := make(map[string][]engine.KeyVariable, 2)
	sections["markov"] = []engine.KeyVariable{
		engine.KeyVariable{"word_limit", &markovWordLimit},
	}
	sections["help"] = []engine.KeyVariable{
		engine.KeyVariable{"rules_file", &RuleFile},
	}
	sections["media"] = []engine.KeyVariable{
		engine.KeyVariable{"allow_explicit", &AllowExplicit},
	}

	engine.LoadConfig(sections)
	InitTime()
	InitMarkov()
	InitAlias()

	engine.RegisterMethod("help", GetHelp, "Show this help page")
	engine.RegisterMethod("rules", GetRules, "Show the channel rules, if any")

	engine.RegisterMethod("search", Search, "Searches the web")
	engine.RegisterMethod("wiki", SearchWikipedia, "Searches for a Wikipedia article")
	engine.RegisterMethod("define", SearchWiktionary, "Searches Wiktionary for a definition")

	engine.RegisterMethod("eval", DoMath, "Evaluate mathematical expression")

	engine.RegisterMethod("choose", Choose, "Pick a random word from the specified ones")
	engine.RegisterMethod("asmr", Whisper, "ᵈⁱˢᵖˡᵃʸˢ ˢᵒᵐᵉ ˢᵐᵃˡˡ ᵗᵉˣᵗ")
	engine.RegisterMethod("scream", Scream, "Ｄｉｓｐｌａｙｓ  ｓｏｍｅ  ｌａｒｇｅ  ｔｅｘｔ")
	engine.RegisterMethod("nice", Nice, "𝓓𝓲𝓼𝓹𝓵𝓪𝔂𝓼 𝓼𝓸𝓶𝓮 𝓷𝓲𝓬𝓮-𝓵𝓸𝓸𝓴𝓲𝓷𝓰 𝓽𝓮𝔁𝓽")
	engine.RegisterMethod("is", Is, "Answers with a yes-ish or no-ish answer")
	engine.RegisterMethod("caesar", Caesar, "Encrypts text in a Caesar cipher")
	engine.RegisterMethod("give", Give, "Invokes a command as someone else")

	engine.RegisterMethod("time", GetTime, "Displays the current time")
	engine.RegisterMethod("getzone", GetTimezone, "Displays the active time zone of the user")
	engine.RegisterMethod("setzone", SetTimezone, "Sets the active time zone of the user")

	engine.RegisterMethod("learn", LearnHTTP, "Teaches the bot using a web page")

	engine.RegisterMethod("markov", GenerateMarkovChain, "Generates a markov chain")
	engine.RegisterMethod("words", GetWordStats, "Displays statistics about the markov chain")
	engine.RegisterMethod("savechain", SaveMarkovChain, "Saves the last generated markov chain")
	engine.RegisterMethod("delchain", DeleteMarkovChain, "Deletes a markov chain")
	engine.RegisterMethod("repeatchain", RepeatMarkovChain, "Replays a markov chain")
	engine.RegisterMethod("listchains", ListMarkovChains, "Lists markov chains")

	engine.RegisterMethod("addalias", AddAlias, "Adds an alias")
	engine.RegisterMethod("editalias", EditAlias, "Edits an alias")
	engine.RegisterMethod("delalias", DelAlias, "Deletes an alias")
	engine.RegisterMethod("listalias", ListAliases, "Lists aliases")
	engine.RegisterMethod("definealias", DefineAlias, "Displays an alias definition")

	engine.RegisterMethod("reverse", Reverse, "Displays text backwards")
	engine.RegisterMethod("repeat", Repeat, "Duplicates text")
	engine.RegisterMethod("count", Count, "Counts the number of letters")
	engine.RegisterMethod("head", Head, "Cuts out the first letters of text")
	engine.RegisterMethod("cuthead", CutHead, "Removes the first letters of text")
	engine.RegisterMethod("tail", Tail, "Cuts out the last letters of text")
	engine.RegisterMethod("cuttail", CutTail, "Removes the last letters of text")

	engine.RegisterMethod("urbandict", UrbanDictionary, "Searches Urban dictionary")
	engine.RegisterMethod("mastodon", Mastodon, "Searches Mastodon")
	engine.RegisterMethod("sofurry", SoFurrySearch, "Searches SoFurry")
	engine.RegisterMethod("furrynet", FurryNetworkSearch, "Searches FurryNetwork")
	engine.RegisterMethod("inkbunny", InkbunnySearch, "Searches Inkbunny")
	engine.RegisterMethod("e621", E621Search, "Searches e621")

	engine.RegisterHook(urlRegex, onUrl)
	engine.RegisterHook(nil, Learn)
	engine.RegisterHook(AliasRegex, OnAlias)
	engine.Start()

	CloseMedia()
}
