package main

import (
	"testing"
)

func TestAliasMethods(context *testing.T) {
	assertUtil(context, Reverse, "testing", "gnitset")
	assertUtil(context, Repeat, "testing", "testingtesting")
	assertUtil(context, Count, "testing", "7")
	assertUtil(context, Head, "4 testing", "test")
	assertUtil(context, CutHead, "4 testing", "ing")
	assertUtil(context, Tail, "3 testing", "ing")
	assertUtil(context, CutTail, "3 testing", "test")
}
