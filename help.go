
package main

import (
	"gitlab.com/Hanicef/fuzzybot/engine"
	"log"
	"os"
	"io/ioutil"
	"strings"
	"time"
)

var HelpURL string = ""
var HelpExpiration time.Time
var RuleFile string = ""
var RuleURL string = ""
var RuleExpiration time.Time

func LoadRules() bool {
	if RuleFile == "" {
		log.Print("WARNING: Rule file not specified in config")
		return false
	}

	stream, err := os.Open(RuleFile)
	if err != nil {
		log.Println("WARNING: Failed to open rule file:", err.Error())
		return false
	}
	defer stream.Close()

	// Load the entire rule file, so we can serve it quickly.
	data, err := ioutil.ReadAll(stream)
	if err != nil {
		log.Println("WARNING: Failed to read rule file:", err.Error())
		return false
	}

	var expires string
	RuleURL, expires, err = createPaste("fuzzyrules", string(data))
	if err != nil {
		log.Println("WARNING: Failed to post rule data:", err.Error())
		return false
	}

	RuleExpiration, err = time.Parse(time.RFC3339, expires)
	if err != nil {
		log.Println("WARNING: Failed to parse rule expiration; expiration date is undefined")
	}
	return true
}

func generateListing() []byte {
	var data []byte
	var count int = 0
	var methods = make([]struct{key string; value engine.Method}, 0, len(engine.Methods))

	for k, v := range engine.Methods {
		// Sort the methods.
		for i := 0; i < cap(methods); i++ {
			if i < len(methods) && strings.Compare(methods[i].key, k) > 0 {
				methods = append(methods, struct{key string; value engine.Method}{k, v})

				for j := len(methods)-1; j > i; j-- {
					methods[j] = methods[j-1]
				}

				methods[i].key = k
				methods[i].value = v
				break
			} else if i >= len(methods) {
				methods = append(methods, struct{key string; value engine.Method}{k, v})
				break
			}
		}

		count += len(k) + len(v.Desc) + 4
	}

	data = make([]byte, 0, len(" ~ Command listing ~\n\n") + count)
	data = append(data, []byte(" ~ Command listing ~\n\n")...)
	for i := range methods {
		data = append(data, []byte(methods[i].key)...)
		data = append(data, ' ', '-', ' ')
		data = append(data, []byte(methods[i].value.Desc)...)
		data = append(data, '\n')
	}

	return data
}

func LoadHelp() bool {
	var expires string
	var err error

	data := generateListing()

	HelpURL, expires, err = createPaste("fuzzyhelp", string(data))
	if err != nil {
		log.Println("WARNING: Failed to post help data:", err.Error())
		return false
	}

	HelpExpiration, err = time.Parse(time.RFC3339, expires)
	if err != nil {
		log.Println("WARNING: Failed to parse help expiration; expiration date is undefined")
	}

	return true
}

func GetHelp(source *engine.Identifier) string {
	if HelpURL == "" {
		if !LoadHelp() {
			return "Can't display help: commands failed to load"
		}
	}
	return HelpURL
}

func GetRules(source *engine.Identifier) string {
	if RuleURL == "" {
		if !LoadRules() {
			return "Can't display rules: rules configured improperly or failed to load"
		}
	}

	return RuleURL
}
