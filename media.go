package main

import (
	"gitlab.com/Hanicef/fuzzybot/engine"
	"encoding/json"
	"strings"
	"log"
	"strconv"
	"fmt"
	"errors"
	"time"
)

var IBSession string
var AllowExplicit = false

var e621RateLimit int
var e621RateTime int64

type UDResult struct {
	List []struct {
		Word string `json:"word"`
		Permalink string `json:"permalink"`
		Definition string `json:"definition"`
	} `json:"list"`
}

type MastoResult struct {
	Id string `json:"id"`
	Url string `json:"url"`
	Account struct {
		DisplayName string `json:"display_name"`
	} `json:"account"`
	Content string `json:"content"`
	SpoilerText string `json:"spoiler_text"`
	Sensitive bool `json:"sensitive"`
}

type SFData struct {
	Data struct {
		Entries []struct {
			Title string `json:"title"`
			ArtistName string `json:"artistName"`
			Full string `json:"full"`
		} `json:"entries"`
	} `json:"data"`
}

type FNData struct {
	Hits []struct {
		Source struct {
			Title string `json:"title"`
			Character struct {
				DisplayName string `json:"display_name"`
			} `json:"character"`
			Images struct {
				Original string `json:"original"`
			} `json:"images"`
		} `json:"_source"`
	} `json:"hits"`
}

type IBData struct {
	ResultsCountAll string `json:"results_count_all"`
	Submissions []struct {
		Username string `json:"username"`
		Title string `json:"title"`
		FileUrlFull string `json:"file_url_full"`
	} `json:"submissions"`
}

type IBSessionData struct {
	Sid string `json:"sid"`
}

type EData struct {
	Id int `json:"id"`
	FileUrl string `json:"file_url"`
	Artist []string `json:"artist"`
}

var escapeReplacer = strings.NewReplacer("&", "%26", "%", "%25", "=", "%3D")

func getJson(object interface{}, url string) error {
	resp, err := Fetch("GET", url, "", nil)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return errors.New("got non-200 status code")
	}

	if object != nil {
		return json.NewDecoder(resp.Body).Decode(object)
	} else {
		return nil
	}
}

func getUser(s string) (string, string) {
	start := strings.IndexByte(s, '@')
	if start == -1 {
		return s, ""
	}

	end := start + 1
	for end < len(s) && s[end] != ' ' {
		end++
	}

	user := s[start+1:end]
	if end < len(s) {
		return s[:start] + s[end+1:], user
	} else {
		return s[:start], user
	}
}

func UrbanDictionary(source *engine.Identifier) string {
	var data UDResult

	// See: https://github.com/zdict/zdict/wiki/Urban-dictionary-API-documentation
	if source.Parameter == nil {
		return "No parameter specified"
	}

	search := escapeReplacer.Replace(*source.Parameter)
	err := getJson(&data, "https://api.urbandictionary.com/v0/define?term=" + search)
	if err != nil {
		return fmt.Sprint("Failed to query Urban Dictionary: ", err.Error())
	}

	if len(data.List) == 0 {
		return "No result"
	} else {
		definition := strings.Replace(data.List[0].Definition, "\r\n", " ", -1)
		if len(data.List[0].Definition) > 64 {
			return fmt.Sprintf("%s - %s... [%s]", data.List[0].Word, definition[:64], data.List[0].Permalink)
		} else {
			return fmt.Sprintf("%s - %s [%s]", data.List[0].Word, definition, data.List[0].Permalink)
		}
	}
}

func mastoFormat(s string) string {
	var out []byte = make([]byte, 0, len(s))
	var j int
	var tag string

	for i := 0; i < len(s); i++ {
		if s[i] == '<' {
			for j = i; j < len(s) && s[j] != '>'; j++ { }
			tag = s[i+1:j]
			if tag == "/p" || tag == "br" {
				out = append(out, ' ')
			}
			i = j
		} else {
			out = append(out, s[i])
		}
	}

	return string(out)
}

func Mastodon(source *engine.Identifier) string {
	var data []MastoResult

	// See: https://docs.joinmastodon.org/api/rest/timelines/#get-api-v1-timelines-tag-hashtag
	if source.Parameter == nil {
		return "No parameter specified"
	}

	tag := escapeReplacer.Replace(*source.Parameter)
	err := getJson(&data, fmt.Sprintf("https://mastodon.social/api/v1/timelines/tag/%s?limit=1", tag))
	if err != nil {
		return fmt.Sprint("Failed to query Mastodon: ", err.Error())
	}

	if len(data) == 0 {
		return "No result"
	} else {
		if !AllowExplicit && (data[0].SpoilerText != "" || data[0].Sensitive) {
			for {
				// Possibly explicit content; keep searching.
				err = getJson(&data, fmt.Sprintf("https://mastodon.social/api/v1/timelines/tag/%s?limit=20&max_id=%s", tag, data[len(data)-1].Id))
				if err != nil {
					return fmt.Sprint("Failed to query Mastodon: ", err.Error())
				}

				for i, r := range data {
					if r.SpoilerText == "" && !r.Sensitive {
						data = data[i:]
						break
					}
				}

				if len(data) < 20 {
					break
				}
			}

			if data[0].SpoilerText != "" || data[0].Sensitive {
				return "No result"
			}
		}

		content := mastoFormat(data[0].Content)
		if len(content) == 0 {
			return "Can't display result: got empty status from Mastodon"
		} else {
			if len(content) > 64 {
				return fmt.Sprintf("%s [%s]: %s...", data[0].Account.DisplayName, data[0].Url, content[:64])
			} else {
				return fmt.Sprintf("%s [%s]: %s", data[0].Account.DisplayName, data[0].Url, content)
			}
		}
	}
}

func SoFurrySearch(source *engine.Identifier) string {
	var data SFData
	var maxlevel = 0

	// See: https://wiki.sofurry.com/wiki/SoFurry_2.0_API#Search_submissions
	if source.Parameter == nil {
		return "No parameter specified"
	}

	if AllowExplicit {
		maxlevel = 2
	}

	search := escapeReplacer.Replace(*source.Parameter)
	err := getJson(&data, fmt.Sprintf("https://api2.sofurry.com/browse/search?format=json&search=%s&maxlevel=%d" + search, maxlevel))
	if err != nil {
		return fmt.Sprint("Failed to query SoFurry: ", err.Error())
	}

	if len(data.Data.Entries) == 0 {
		return "No result"
	} else {
		return fmt.Sprintf("%s - %s [%s]", data.Data.Entries[0].ArtistName, data.Data.Entries[0].Title, data.Data.Entries[0].Full)
	}
}

func FurryNetworkSearch(source *engine.Identifier) string {
	var data FNData

	// NOTE: The API is reversed-engineered from the website through it's JS code.
	// Visit https://beta.furrynetwork.com/ and pay attention to the JS fetches for reference.
	if source.Parameter == nil {
		return "No parameter specified"
	}

	// TODO: Figure out how to control ratings.

	search, user := getUser(escapeReplacer.Replace(*source.Parameter))
	var err error
	if user != "" {
		if search == "" {
			err = getJson(&data, "https://beta.furrynetwork.com/api/search/artwork/" + user)
		} else {
			err = getJson(&data, fmt.Sprintf("https://beta.furrynetwork.com/api/search/artwork/%s?tags[]=%s", user, strings.Replace(search, " ", "&tags[]=", -1)))
		}
	} else {
		err = getJson(&data, "https://beta.furrynetwork.com/api/search/artwork?tags[]=" + strings.Replace(search, " ", "&tags[]=", -1))
	}

	if err != nil {
		return fmt.Sprint("Failed to query FurryNetwork: ", err.Error())
	}

	if len(data.Hits) == 0 {
		return "No result"
	} else {
		return fmt.Sprintf("%s - %s [%s]", data.Hits[0].Source.Character.DisplayName, data.Hits[0].Source.Title, data.Hits[0].Source.Images.Original)
	}
}

func InkbunnySearch(source *engine.Identifier) string {
	var data IBData
	var err error

	// See: https://wiki.inkbunny.net/wiki/API#Search
	if source.Parameter == nil {
		return "No parameter specified"
	}

	if IBSession == "" {
		var exp = "no"

		// Inkbunny requires a 'session ID' for calls to work.
		// But since we don't need any user-specific calls, a guest one is sufficient.
		var sdata IBSessionData
		err = getJson(&sdata, "https://inkbunny.net/api_login.php?username=guest")
		if err != nil {
			return fmt.Sprint("Failed to create Inkbunny session: ", err.Error())
		}

		IBSession = sdata.Sid

		if AllowExplicit {
			exp = "yes"
		}

		err = getJson(&sdata, fmt.Sprintf("https://inkbunny.net/api_userrating.php?sid=%s&tag[2]=%s&tag[3]=%s&tag[4]=%s&tag[5]=%s", sdata.Sid, exp, exp, exp, exp))
		if err != nil {
			return fmt.Sprint("Failed to set session rating: ", err.Error())
		}

		IBSession = sdata.Sid
	}

	// Cut out @username if there is any.
	search, user := getUser(escapeReplacer.Replace(*source.Parameter))
	if user != "" {
		err = getJson(&data, fmt.Sprintf("https://inkbunny.net/api_search.php?sid=%s&submissions_per_page=1&title=yes&text=%s&username=%s", IBSession, search, user))
	} else {
		err = getJson(&data, fmt.Sprintf("https://inkbunny.net/api_search.php?sid=%s&submissions_per_page=1&title=yes&text=%s", IBSession, search))
	}
	if err != nil {
		return fmt.Sprint("Failed to query Inkbunny: ", err.Error())
	}

	count, err := strconv.Atoi(data.ResultsCountAll)
	if err != nil {
		return fmt.Sprint("Failed to parse result count: ", err.Error())
	}

	if count == 0 {
		return "No results"
	} else {
		return fmt.Sprintf("%s - %s [%s]", data.Submissions[0].Username, data.Submissions[0].Title, data.Submissions[0].FileUrlFull)
	}
}

func E621Search(source *engine.Identifier) string {
	var data []EData
	var err error
	var explicit string

	// See: https://e621.net/help/show/api
	if source.Parameter == nil {
		return "No parameter specified"
	}

	if time.Now().Unix() > e621RateTime + 1 {
		// e621 is rate limited at 2 requests/second. Enforce this limit.
		e621RateTime = time.Now().Unix()
		e621RateLimit = 1
	} else {
		e621RateLimit++
		if e621RateLimit > 2 {
			return "Can't send any more requests: rate limit reached"
		}
	}

	if !AllowExplicit {
		explicit = "+rating:s"
	}

	search := strings.Replace(escapeReplacer.Replace(*source.Parameter), " ", "+", -1)
	err = getJson(&data, fmt.Sprintf("https://e621.net/post/index.json?limit=1&tags=%s%s", search, explicit))
	if err != nil {
		return fmt.Sprint("Failed to query e621: ", err.Error())
	}

	if len(data) == 0 {
		return "No results"
	} else {
		if len(data[0].Artist) == 0 {
			return fmt.Sprintf("Unknown - %d [%s]", data[0].Id, data[0].FileUrl)
		}

		return fmt.Sprintf("%s - %d [%s]", data[0].Artist[0], data[0].Id, data[0].FileUrl)
	}
}

func CloseMedia() {
	if IBSession != "" {
		err := getJson(nil, "https://inkbunny.net/api_logout.php?sid=" + IBSession)
		if err != nil {
			log.Println("WARNING: Inkbunny session", IBSession, "failed to close!")
		}
	}
}
