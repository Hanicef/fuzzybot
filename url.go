
package main

import (
	"gitlab.com/Hanicef/fuzzybot/engine"
	"regexp"
	"bytes"
	"strings"
	"io"
	"log"
	"fmt"
	"errors"
)

func extractTag(tag []byte) []byte {
	return bytes.ToLower(bytes.Split(tag, []byte{' '})[0])
}

func parseTag(stream io.Reader) ([]byte, error) {
	var err error
	var tag []byte
	var char [1]byte

	for {
		_, err = stream.Read(char[:])
		if err != nil {
			if err == io.EOF {
				return nil, errors.New("unexpected EOF in tag")
			} else {
				return nil, err
			}
		}

		if char[0] == '>' {
			return tag, nil
		}

		if char[0] != ' ' && char[0] != '\t' && char[0] != '\n' {
			tag = append(tag, char[0])
		}
	}
}

func findTitle(data io.Reader) string {
	var b [1]byte
	var err error
	var tag []byte
	var out []byte = make([]byte, 0, 8)

	for {
		_, err = data.Read(b[:])
		if err != nil {
			return ""
		}

		if b[0] == '<' {
			tag, err = parseTag(data)
			if err != nil {
				return ""
			}

			if bytes.Compare(extractTag(tag), []byte("title")) == 0 {
				for {
					_, err = data.Read(b[:])
					if err != nil {
						return ""
					}

					if b[0] == '<' {
						tag, err = parseTag(data)
						if err != nil {
							return ""
						}

						if string(extractTag(tag)) == "/title" {
							return string(out)
						} else {
							// Tags within title is not allowed.
							return ""
						}
					}
					out = append(out, b[0])
				}
			}
		}
	}
}

const (
	uriUnreserved = `[:alnum:]\-\._~!\$&'\(\)\*\+,;=`
	uriPctEncoded = `%[\da-fA-F]{2}`
	uriPchar = `(?:[` + uriUnreserved + `\:@]|` + uriPctEncoded + `)`
	uriRegName = `(?:[` + uriUnreserved + `]|` + uriPctEncoded + `)`

	ipv6Digit = `[\da-fA-F]{0,4}`
	ipv6Subregex = `(?:` + ipv6Digit + `\:)*(?:` + ipv6Digit + `)*`

	uriHost = `(?:` + uriRegName + `+|\[` + ipv6Subregex + `\])`
	uriPort = `\d+`
	uriAuthority = uriHost + `(?:\:` + uriPort + ")?"

	uriPath = `(?:/` + uriPchar + `*)*`
	uriQuery = `(?:\?` + uriPchar + `*)?`
	uriFragment = `(?:#` + uriPchar + `*)?`
)

// This is a conforming HTTP URL as per https://tools.ietf.org/html/rfc3986
var urlRegex = regexp.MustCompile(`https?://(` + uriAuthority + `)` + uriPath + uriQuery + uriFragment)
func onUrl(source *engine.Identifier, matches []string) string {
	if matches[1] == "[::1]" || matches[1] == "127.0.0.1" || matches[1] == "localhost" {
		return fmt.Sprintf("%s - ᕙ( ͡° ͜ʖ ͡° )ᕗ", matches[1])
	}

	resp, err := Fetch("GET", matches[0], "", nil)
	if err != nil {
		// Dont bother parsing if we can't parse it.
		log.Print("WARNING: GET failed on ", matches[0], " with error: ", err.Error())
		return ""
	}
	defer resp.Body.Close()

	contentType := strings.Split(resp.Header.Get("Content-Type"), ";")[0]
	if contentType != "text/html" {
		return ""
	}

	title := findTitle(resp.Body)
	if title != "" {
		return fmt.Sprintf("%s - %s", matches[1], title)
	}
	return ""
}
