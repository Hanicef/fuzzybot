
package main

import (
	"gitlab.com/Hanicef/fuzzybot/engine"
	"bytes"
	"log"
	"time"
	"fmt"
	"errors"
	"strconv"
	"database/sql"
)

type MarkovLimit struct {
	time int64
	count int
}

type LastGeneration struct {
	base string
	seed int64
}

type NextCandidate struct {
	next int64
	occur int64
}

var markovWordLimit int64 = 20
var markovSeed int64

var lastGens map[string]LastGeneration = make(map[string]LastGeneration, 1)
var markovLimit map[string]*MarkovLimit = make(map[string]*MarkovLimit, 1)

func InitMarkov() {
	_, err := engine.Database.Exec("CREATE TABLE IF NOT EXISTS Word(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, word STRING UNIQUE NOT NULL, occur INT NOT NULL)")
	if err != nil {
		log.Fatal("ERROR:", err.Error())
	}
	_, err = engine.Database.Exec("CREATE TABLE IF NOT EXISTS WordConnection(prev INTEGER REFERENCES Word(id), next INTEGER REFERENCES Word(id), occur INT NOT NULL)")
	if err != nil {
		log.Fatal("ERROR:", err.Error())
	}
	_, err = engine.Database.Exec("CREATE TABLE IF NOT EXISTS StartWord(word INTEGER REFERENCES Word(id), occur INT NOT NULL)")
	if err != nil {
		log.Fatal("ERROR:", err.Error())
	}

	_, err = engine.Database.Exec("CREATE TABLE IF NOT EXISTS SavedChain(name STRING PRIMARY KEY NOT NULL, seed INTEGER NOT NULL, base STRING)")
	if err != nil {
		log.Fatal("ERROR:", err.Error())
	}
}

func fastRandom(limit int64) int64 {
	// The markov chain is sluggish asf, so use a fast RNG
	// See: https://en.wikipedia.org/wiki/Xorshift
	markovSeed ^= markovSeed << 13
	markovSeed ^= markovSeed >> 7
	markovSeed ^= markovSeed << 17
	return (markovSeed & 0x7fffffffffffffff) % limit
}

func generateSequence(active int64, limit int64, forward bool) ([]byte, int64, error) {
	// Allocate a decent chunk of memory to give the markov chain a head start.
	var sentence []byte = make([]byte, 0, 64)
	var word []byte
	var occur int64

	var row *sql.Row
	var rows *sql.Rows
	var next int64
	var choice int64
	var err error
	var hasNext bool

	var i int64 = 0
	for ; i < limit; i++ {
		row = engine.Database.QueryRow("SELECT word, occur FROM Word WHERE id = $1", active)
		err = row.Scan(&word, &occur)
		if err != nil {
			return nil, 0, errors.New("SELECT Word: " + err.Error())
		}

		if forward {
			sentence = append(sentence, word...)
			sentence = append(sentence, ' ')
		} else if i > 0 {
			if sentence == nil {
				sentence = word
			} else {
				sentence = bytes.Join([][]byte{word, sentence}, []byte{' '})
			}
		}

		if forward {
			rows, err = engine.Database.Query("SELECT next, occur FROM WordConnection WHERE prev = $1", active)
			if err != nil {
				return nil, 0, errors.New("SELECT WordConnection: " + err.Error())
			}
		} else {
			rows, err = engine.Database.Query("SELECT prev, occur FROM WordConnection WHERE next = $1", active)
			if err != nil {
				return nil, 0, errors.New("SELECT WordConnection: " + err.Error())
			}
		}

		// Respect the value in occur; the higher it is, the more likely it should be of appearing.
		choice = fastRandom(occur)
		for choice >= 0 {
			hasNext = rows.Next()
			if !hasNext {
				return sentence, i, rows.Err()
			}

			err = rows.Scan(&next, &occur)
			if err != nil {
				return nil, 0, errors.New("Scan: " + err.Error())
			}

			choice -= occur
		}

		err = rows.Close()
		if err != nil {
			return nil, 0, errors.New("Close: " + err.Error())
		}

		active = next
	}

	return sentence, i, nil
}

func generate(root *string, seed int64) ([]byte, error) {
	var count int64
	var start int64
	var occur int64

	// Initiate the markov chain
	markovSeed = seed

	engine.LockDatabase()
	defer engine.UnlockDatabase()
	if root == nil {
		row := engine.Database.QueryRow("SELECT SUM(occur) FROM StartWord")
		err := row.Scan(&count)
		if err != nil {
			return nil, errors.New("generate: " + err.Error())
		}

		rows, err := engine.Database.Query("SELECT word, occur FROM StartWord")
		if err != nil {
			return nil, errors.New("generate: " + err.Error())
		}

		choice := fastRandom(count)
		for choice >= 0 && rows.Next() {
			err = rows.Scan(&start, &occur)
			if err != nil {
				return nil, errors.New("generate: " + err.Error())
			}

			choice -= occur
		}

		err = rows.Close()
		if err != nil {
			return nil, errors.New("generate: " + err.Error())
		}

		sequence, _, err := generateSequence(start, markovWordLimit, true)
		return sequence, err
	} else {
		// Generate a word containing a specific word.
		row := engine.Database.QueryRow("SELECT id FROM Word WHERE word = $1", *root)
		err := row.Scan(&start)
		if err != nil {
			if err == sql.ErrNoRows {
				// The word specified is not known.
				return nil, nil
			} else {
				return nil, errors.New("generate: " + err.Error())
			}
		}

		begin, count, err := generateSequence(start, markovWordLimit, false)
		if err != nil {
			return nil, errors.New("generate: " + err.Error())
		}

		end, _, err := generateSequence(start, markovWordLimit - count, true)
		return append(begin, end...), err
	}
}

func GenerateMarkovChain(source *engine.Identifier) string {
	now := time.Now().Unix()
	if source.Parameter == nil {
		lastGens[source.Source] = LastGeneration{"", now}
	} else {
		lastGens[source.Source] = LastGeneration{*source.Parameter, now}
	}

	sentence, err := generate(source.Parameter, now)
	if err != nil {
		return fmt.Sprint("Failed to generate markov chain: ", err.Error())
	}

	if sentence == nil {
		return "This word is not known"
	} else {
		return string(sentence)
	}
}

func GetWordStats(source *engine.Identifier) string {
	engine.LockDatabase()
	defer engine.UnlockDatabase()

	if source.Parameter == nil {
		var wordcount int64
		var wordsum int64
		var connections int64
		var starts int64

		row := engine.Database.QueryRow("SELECT COUNT(*), SUM(occur) FROM Word")
		err := row.Scan(&wordcount, &wordsum)
		if err != nil {
			return fmt.Sprint("Failed to gather word statistics: ", err.Error())
		}

		row = engine.Database.QueryRow("SELECT COUNT(*) FROM WordConnection")
		err = row.Scan(&connections)
		if err != nil {
			return fmt.Sprint("Failed to gather word statistics: ", err.Error())
		}

		row = engine.Database.QueryRow("SELECT COUNT(*) FROM StartWord")
		err = row.Scan(&starts)
		if err != nil {
			return fmt.Sprint("Failed to gather word statistics: ", err.Error())
		}

		return fmt.Sprintf("Known words: %d, seen words: %d, known starting words: %d, knows sequences: %d", wordcount, wordsum, starts, connections)
	} else {
		var count int64
		row := engine.Database.QueryRow("SELECT COUNT(*) FROM Word WHERE word = $1", *source.Parameter)
		err := row.Scan(&count)
		if err != nil {
			return fmt.Sprint("Failed to gather word statistics: ", err.Error())
		}

		return fmt.Sprintf("%s: seen %d times", *source.Parameter, count)
	}
}

func SaveMarkovChain(source *engine.Identifier) string {
	var name string

	if source.Parameter == nil {
		return "No name specified"
	}

	chain, ok := lastGens[source.Source]
	if !ok {
		return "Can't save when no chain has been generated"
	}

	engine.LockDatabase()
	defer engine.UnlockDatabase()

	row := engine.Database.QueryRow("SELECT name FROM SavedChain WHERE name = $1", *source.Parameter)
	err := row.Scan(&name)
	if err != nil {
		if err == sql.ErrNoRows {
			if chain.base == "" {
				_, err = engine.Database.Exec("INSERT INTO SavedChain(name, seed, base) VALUES ($1, $2, $3)", *source.Parameter, chain.seed, nil)
			} else {
				_, err = engine.Database.Exec("INSERT INTO SavedChain(name, seed, base) VALUES ($1, $2, $3)", *source.Parameter, chain.seed, chain.base)
			}
			if err != nil {
				return fmt.Sprint("Failed to save markov chain: ", err.Error())
			} else {
				return "Done"
			}
		} else {
			return fmt.Sprint("Failed to save markov chain: ", err.Error())
		}
	}

	return fmt.Sprint("Chain ", name, " already exists")
}

func DeleteMarkovChain(source *engine.Identifier) string {
	if source.Parameter == nil {
		return "No name specified"
	}

	if source.Nick.Mode < engine.ModeOp {
		return "Insufficient privileges to delete markov chain"
	}

	engine.LockDatabase()
	defer engine.UnlockDatabase()

	res, err := engine.Database.Exec("DELETE FROM SavedChain WHERE name = $1", *source.Parameter)
	if err != nil {
		return fmt.Sprint("Error while deleting chain: ", err.Error())
	}

	count, err := res.RowsAffected()
	if err != nil {
		return fmt.Sprint("Error while querying affection: ", err.Error())
	}

	if count > 0 {
		return "Done"
	} else {
		return fmt.Sprint("Chain ", *source.Parameter, " doesn't exist")
	}
}

func RepeatMarkovChain(source *engine.Identifier) string {
	var seed int64
	var base *string

	if source.Parameter == nil {
		return "No name specified"
	}

	engine.LockDatabase()

	row := engine.Database.QueryRow("SELECT seed, base FROM SavedChain WHERE name = $1", *source.Parameter)
	err := row.Scan(&seed, &base)
	if err != nil {
		if err == sql.ErrNoRows {
			return fmt.Sprint("Chain ", *source.Parameter, " doesn't exist")
		} else {
			return fmt.Sprint("Error while loading chain: ", err.Error())
		}
		engine.UnlockDatabase()
	}

	engine.UnlockDatabase()

	sequence, err := generate(base, seed)
	if err != nil {
		return fmt.Sprint("Error while repeating chain: ", err.Error())
	}

	return string(sequence)
}

func ListMarkovChains(source *engine.Identifier) string {
	var query string
	var name string

	if source.Parameter == nil {
		query = "%"
	} else {
		query = fmt.Sprint("%", *source.Parameter, "%")
	}

	engine.LockDatabase()
	defer engine.UnlockDatabase()

	rows, err := engine.Database.Query("SELECT name FROM SavedChain WHERE name LIKE $1", query)
	if err != nil {
		return fmt.Sprint("Failed to list chains: ", err.Error())
	}

	var out []byte = []byte("Matching chain(s) are ")
	i := 0
	for ; i < 20 && rows.Next(); i++ {
		err = rows.Scan(&name)
		if err != nil {
			return fmt.Sprint("Failed to list chains: ", err.Error())
		}

		out = append(out, name...)
		out = append(out, ',', ' ')
	}

	out = out[:len(out)-2]
	if i < 20 {
		err = rows.Err()
		if err != nil {
			return fmt.Sprint("Failed to list chains: ", err.Error())
		}

		if i == 0 {
			return fmt.Sprint("No chains containing ", *source.Parameter)
		}
	} else {
		err = rows.Close()
		if err != nil {
			return fmt.Sprint("Failed to list chains: ", err.Error())
		}

		out = append(out, []byte("... (")...)
		out = append(out, []byte(strconv.Itoa(i))...)
		out = append(out, []byte(" more)")...)
	}
	return string(out)
}
