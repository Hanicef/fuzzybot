
package main

import (
	"gitlab.com/Hanicef/fuzzybot/engine"
	"math"
	"fmt"
	"errors"
	"strconv"
	"strings"
)

const (
	uvone    = 0x3FF0000000000000
	mask     = 0x7FF
	shift    = 64 - 11 - 1
	bias     = 1023
	signMask = 1 << 63
	fracMask = 1<<shift - 1
)

const (
	mathNone = iota
	mathNumber
	mathPlus
	mathMinus
	mathMultiply
	mathDivide
	mathModulo
	mathExponent
	mathSubsection
	mathFunction
	mathConstant
)

type mathOperator struct {
	Type int
	Value interface{}
}

type mathFunctionSection struct {
	Name string
	Parameters [][]mathOperator
}

// Taken from https://golang.org/src/math/floor.go?s=1237:1266#L54
func round(x float64) float64 {
	bits := math.Float64bits(x)
	e := uint(bits>>shift) & mask
	if e < bias {
		// Round abs(x) < 1 including denormals.
		bits &= signMask // +-0
		if e == bias-1 {
			bits |= uvone // +-1
		}
	} else if e < bias+shift {
		// Round any abs(x) >= 1 containing a fractional component [0,1).
		//
		// Numbers with larger exponents are returned unchanged since they
		// must be either an integer, infinity, or NaN.
		const half = 1 << (shift - 1)
		e -= bias
		bits += half >> e
		bits &^= fracMask >> e
	}
	return math.Float64frombits(bits)
}

func invoke1(name string, function func (float64) float64, args ...float64) (float64, error) {
	if len(args) != 1 {
		return 0.0, fmt.Errorf("invalid argument count for %s(); expected 1", name)
	}

	return function(args[0]), nil
}

func invoke2(name string, function func (float64, float64) float64, args ...float64) (float64, error) {
	if len(args) != 2 {
		return 0.0, fmt.Errorf("invalid argument count for %s(); expected 2", name)
	}

	return function(args[0], args[1]), nil
}

var mathFunctions = map[string] func (...float64) (float64, error) {
	"abs": func (args ...float64) (float64, error) {
		return invoke1("abs", math.Abs, args...)
	},
	"acos": func (args ...float64) (float64, error) {
		return invoke1("acos", math.Acos, args...)
	},
	"acosh": func (args ...float64) (float64, error) {
		return invoke1("acosh", math.Acosh, args...)
	},
	"asin": func (args ...float64) (float64, error) {
		return invoke1("asin", math.Asin, args...)
	},
	"asinh": func (args ...float64) (float64, error) {
		return invoke1("asinh", math.Asinh, args...)
	},
	"atan": func (args ...float64) (float64, error) {
		return invoke1("atan", math.Atan, args...)
	},
	"atan2": func (args ...float64) (float64, error) {
		return invoke1("atan2", math.Atanh, args...)
	},
	"atanh": func (args ...float64) (float64, error) {
		return invoke1("atanh", math.Atanh, args...)
	},
	"cbrt": func (args ...float64) (float64, error) {
		return invoke1("cbrt", math.Cbrt, args...)
	},
	"ceil": func (args ...float64) (float64, error) {
		return invoke1("ceil", math.Ceil, args...)
	},
	"cos": func (args ...float64) (float64, error) {
		return invoke1("cos", math.Cos, args...)
	},
	"cosh": func (args ...float64) (float64, error) {
		return invoke1("cosh", math.Cosh, args...)
	},
	"floor": func (args ...float64) (float64, error) {
		return invoke1("floor", math.Floor, args...)
	},
	"hypot": func (args ...float64) (float64, error) {
		return invoke2("hypot", math.Hypot, args...)
	},
	"log": func (args ...float64) (float64, error) {
		return invoke1("log", math.Log, args...)
	},
	"log10": func (args ...float64) (float64, error) {
		return invoke1("log10", math.Log10, args...)
	},
	"log2": func (args ...float64) (float64, error) {
		return invoke1("log2", math.Log2, args...)
	},
	"min": func (args ...float64) (float64, error) {
		return invoke2("min", math.Min, args...)
	},
	"max": func (args ...float64) (float64, error) {
		return invoke2("max", math.Max, args...)
	},
	"mod": func (args ...float64) (float64, error) {
		return invoke2("mod", math.Mod, args...)
	},
	"round": func (args ...float64) (float64, error) {
		return invoke1("round", round, args...)
	},
	"sin": func (args ...float64) (float64, error) {
		return invoke1("sin", math.Sin, args...)
	},
	"sinh": func (args ...float64) (float64, error) {
		return invoke1("sinh", math.Sinh, args...)
	},
	"sqrt": func (args ...float64) (float64, error) {
		return invoke1("sqrt", math.Sqrt, args...)
	},
	"tan": func (args ...float64) (float64, error) {
		return invoke1("tan", math.Tan, args...)
	},
	"tanh": func (args ...float64) (float64, error) {
		return invoke1("tanh", math.Tanh, args...)
	},
	"trunc": func (args ...float64) (float64, error) {
		return invoke1("trunc", math.Trunc, args...)
	},
}

var mathConstants = map[string]float64 {
	"e": math.E,
	"pi": math.Pi,
	"phi": math.Phi,
}

func isDigit(c byte) bool {
	return (c >= '0' && c <= '9') || c == '.'
}

func isLetter(c byte) bool {
	return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')
}

func parseMath(eval string) ([]mathOperator, error) {
	var section []mathOperator
	var i, j, k int = 0, 0, 0

	for {
		for i < len(eval) && (eval[i] == ' ' || eval[i] == '\t') {
			i++
		}

		if i == len(eval) {
			break
		}

		if isDigit(eval[i]) {
			for j = i+1; j < len(eval) && isDigit(eval[j]); j++ { }

			// Don't bother checking errors; we know this is valid.
			num, _ := strconv.ParseFloat(eval[i:j], 64)
			section = append(section, mathOperator{mathNumber, num})
			i = j
		} else if isLetter(eval[i]) {
			for j = i; j < len(eval) && isLetter(eval[j]); j++ { }
			name := eval[i:j]

			for ; j < len(eval) && (eval[j] == ' ' || eval[j] == '\t'); j++ {}
			if j < len(eval) && eval[j] == '(' {
				// Opening bracket after name; this is a function.
				for k = j; k < len(eval) && eval[k] != ')'; k++ { }
				if k == len(eval) {
					return nil, errors.New("EOL while parsing paranthesis")
				}

				function := mathFunctionSection{name, nil}
				subsections := strings.Split(eval[j+1:k], ",")
				for _, s := range subsections {
					subsection, err := parseMath(s)
					if err != nil {
						return nil, err
					}

					function.Parameters = append(function.Parameters, subsection)
				}

				section = append(section, mathOperator{mathFunction, function})
				j = k+1
			} else {
				// No opening bracket after name; this is a constant.
				section = append(section, mathOperator{mathConstant, name})
			}

			i = j
		} else if eval[i] == '(' {
			for j = i; j < len(eval) && eval[j] != ')'; j++ { }
			if j == len(eval) {
				return nil, errors.New("EOL while parsing paranthesis")
			}

			subsection, err := parseMath(eval[i+1:j])
			if err != nil {
				return nil, err
			}

			section = append(section, mathOperator{mathSubsection, subsection})
			i = j+1
		} else {
			for j = i; j < len(eval) && eval[j] != ' ' && eval[j] != '\t' && !isDigit(eval[j]) && !isLetter(eval[j]) && eval[j] != '('; j++ { }
			if i == j {
				return nil, errors.New("internal error: cannot process empty operator")
			}

			switch eval[i:j] {
				case "+":
					section = append(section, mathOperator{mathPlus, nil})
				case "-":
					section = append(section, mathOperator{mathMinus, nil})
				case "*":
					section = append(section, mathOperator{mathMultiply, nil})
				case "/":
					section = append(section, mathOperator{mathDivide, nil})
				case "%":
					section = append(section, mathOperator{mathModulo, nil})
				case "**":
					section = append(section, mathOperator{mathExponent, nil})
				default:
					return nil, fmt.Errorf("invalid operator %s", eval[i:j])
			}
			i = j
		}
	}

	return section, nil
}

func perform(section []mathOperator, operator string, base int, f func (float64, float64) float64) error {
	var operands [2]*mathOperator
	var i int

	if base == 0 || base == len(section)-1 {
		return fmt.Errorf("missing operand for operator %s", operator)
	}

	// Values not present are filtered out with mathNone.
	for i = base-1; i >= 0 && section[i].Type == mathNone; i-- {}
	if section[i].Type != mathNumber {
		return fmt.Errorf("non-number operand around operator %s", operator)
	}

	operands[0] = &section[i]

	for i = base+1; i < len(section) && section[i].Type == mathNone; i++ {}
	if section[i].Type != mathNumber {
		return fmt.Errorf("non-number operand around operator %s", operator)
	}

	operands[1] = &section[i]

	operands[0].Value = f(operands[0].Value.(float64), operands[1].Value.(float64))
	section[base].Type = mathNone
	operands[1].Type = mathNone

	return nil
}

func evalParanthesis(section []mathOperator) error {
	var err error
	var ok bool
	var method func (...float64) (float64, error)
	var con float64

	for i := 0; i < len(section); i++ {
		if section[i].Type == mathFunction {
			var param []float64
			function := section[i].Value.(mathFunctionSection)

			for _, p := range function.Parameters {
				v, err := evaluate(p)
				if err != nil {
					return err
				}

				param = append(param, v)
			}

			section[i].Type = mathNumber
			method, ok = mathFunctions[function.Name]
			if !ok {
				return fmt.Errorf("no such function %s", function.Name)
			}

			section[i].Value, err = method(param...)
			if err != nil {
				return err
			}

		} else if section[i].Type == mathSubsection {
			section[i].Type = mathNumber
			section[i].Value, err = evaluate(section[i].Value.([]mathOperator))
			if err != nil {
				return err
			}
		} else if section[i].Type == mathConstant {
			section[i].Type = mathNumber
			con, ok = mathConstants[section[i].Value.(string)]
			if !ok {
				return fmt.Errorf("invalid constant %v", section[i].Value.(string))
			}
			section[i].Value = con
		}
	}

	return nil
}

func evalUnaryPlusMinus(section []mathOperator) {
	for i := 0; i < len(section)-1; i++ {
		// Unary plus and minus have a plus or minus between an operator and number.
		if section[i].Type == mathPlus {
			if section[i+1].Type == mathNumber && (i == 0 || section[i-1].Type != mathNumber) {
				section[i+1].Type = mathNone
				section[i].Type = mathNumber
				section[i].Value = section[i+1].Value.(float64)
			}
		} else if section[i].Type == mathMinus {
			if section[i+1].Type == mathNumber && (i == 0 || section[i-1].Type != mathNumber) {
				section[i+1].Type = mathNone
				section[i].Type = mathNumber
				section[i].Value = -section[i+1].Value.(float64)
			}
		}
	}
}

func evalExponent(section []mathOperator) error {
	var err error

	for i := 0; i < len(section); i++ {
		if section[i].Type == mathExponent {
			err = perform(section, "**", i, math.Pow)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func evalMultiplier(section []mathOperator) error {
	var err error

	for i := 0; i < len(section); i++ {
		if section[i].Type == mathMultiply {
			err = perform(section, "*", i, func (f1, f2 float64) float64 {
				return f1 * f2
			})
		} else if section[i].Type == mathDivide {
			err = perform(section, "/", i, func (f1, f2 float64) float64 {
				return f1 / f2
			})
		} else if section[i].Type == mathModulo {
			err = perform(section, "%", i, math.Mod)
		}

		if err != nil {
			return err
		}
	}

	return nil
}

func evalAddition(section []mathOperator) error {
	var err error

	for i := 0; i < len(section); i++ {
		if section[i].Type == mathPlus {
			err = perform(section, "+", i, func (f1, f2 float64) float64 {
				return f1 + f2
			})
		} else if section[i].Type == mathMinus {
			err = perform(section, "-", i, func (f1, f2 float64) float64 {
				return f1 - f2
			})
		}

		if err != nil {
			return err
		}
	}

	return nil
}

func evaluate(section []mathOperator) (float64, error) {
	// Priority order (highest to lowest):
	// ()
	// unary + -
	// **
	// % / *
	// + -
	var err error

	if len(section) == 0 {
		return 0.0, errors.New("nothing to evaluate")
	}

	err = evalParanthesis(section)
	if err != nil {
		return 0.0, err
	}

	evalUnaryPlusMinus(section)
	err = evalExponent(section)
	if err != nil {
		return 0.0, err
	}

	err = evalMultiplier(section)
	if err != nil {
		return 0.0, err
	}

	err = evalAddition(section)
	if err != nil {
		return 0.0, err
	}

	// All sections should be none, except section 0 which should be number.
	for i := range section {
		if i == 0 && section[i].Type != mathNumber {
			return 0.0, errors.New("internal error: non-number result from evaluation")
		} else if i != 0 && section[i].Type != mathNone {
			return 0.0, errors.New("internal error: not all sections evaluated")
		}
	}

	return section[0].Value.(float64), nil
}

func DoMath(source *engine.Identifier) string {
	if source.Parameter == nil {
		return "42"
	}

	section, err := parseMath(*source.Parameter)
	if err != nil {
		return fmt.Sprint("Syntax error: ", err)
	}

	result, err := evaluate(section)
	if err != nil {
		return fmt.Sprint("Syntax error: ", err)
	}

	return strconv.FormatFloat(result, 'f', -1, 64)
}
