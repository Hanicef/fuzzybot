# Fuzzybot - Utility bot for IRC, written in Go

## Build

Go version 1.7 or later is required to build this bot. To build, simply issue:

```
go get gitlab.com/Hanicef/fuzzybot
```

This will generate a file under your home directory named `go/bin`, which
contains the executable to run this bot.

## Configure

Before the bot can be ran, the bot needs a configuration file. It can be placed
anywhere, but unless explicitly specified, the bot will by default search for a
config file at `~/.fuzzybot/config`.

The config file is a INI-formatted file, and has the following sections and keys:

### Section connection

| Key | Description | Default |
| --- | ----------- | ------- |
| nick | The nickname of the bot | FuzzyBot |
| user | The username of the bot | fuzzybot |
| name | The IRC name of the bot | FuzzyBot, by Hanicef |
| server | The server the bot will connect to | - |
| channel | The channel to join when the bot has connected | - |
| use\_ssl | Indicates whenever the bot should connect with SSL | false |
| use\_nickserv | Indicates whenever the bot identify with NickServ | false |
| password | Password to use for NickServ identification | - |

### Section bot

| Key | Description | Default |
| --- | ----------- | ------- |
| prefix | The prefix to use for commands | . |
| invalid\_respond | Whenever the bot should respond to invalid commands | true |
| chat\_log | The file to log chat to, defaults to stdout if absent | - |
| log\_size\_limit | If `chat_log` is specified, limit the log file size to this value, or unlimited if 0 | 0 |

### Section markov

| Key | Description | Default |
| --- | ----------- | ------- |
| word\_limit | Generation word limit for Markov chain | 20 |

### Section help

| Key | Description | Default |
| --- | ----------- | ------- |
| rules\_file | File to use for channel rules | - |

### Section media

| Key | Description | Default |
| --- | ----------- | ------- |
| allow\_explicit | Whetever explicit content should be allowed | false |

## Run

To run, simply start the executable file. The bot also takes these parameters:

| Parameter | Description |
| --------- | ----------- |
| -config | Path to the configuration file |
| -database | Path to the database file |
