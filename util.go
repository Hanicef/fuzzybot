
package main

import (
	"gitlab.com/Hanicef/fuzzybot/engine"
	"strings"
	"strconv"
	"math/rand"
	"unicode/utf8"
)

var answers = []string{
	"Of course!",
	"Absolutely not!",
	"I don't know.",
	"I'm not sure.",
	"Maybe.",
	"Maybe not.",
	"Yes.",
	"No.",
	"Probably.",
	"Probably not.",
}

var lowerNumbers = []rune{
	'⁰',
	'¹',
	'²',
	'³',
	'⁴',
	'⁵',
	'⁶',
	'⁷',
	'⁸',
	'⁹',
}

var lowerLetters = []rune{
	'ᵃ',
	'ᵇ',
	'ᶜ',
	'ᵈ',
	'ᵉ',
	'ᶠ',
	'ᵍ',
	'ʰ',
	'ⁱ',
	'ʲ',
	'ᵏ',
	'ˡ',
	'ᵐ',
	'ⁿ',
	'ᵒ',
	'ᵖ',
	'ˁ',
	'ʳ',
	'ˢ',
	'ᵗ',
	'ᵘ',
	'ᵛ',
	'ʷ',
	'ˣ',
	'ʸ',
	'ᶻ',
}

func toRune(s string, i int) rune {
	// See: https://www.unicode.org/versions/Unicode11.0.0/UnicodeStandard-11.0.pdf
	// 3.9 Unicode Encoding Forms
	if (s[i] & 0x80) == 0 {
		return rune(s[i])
	} else if (s[i] & 0xe0) == 0xc0 {
		if len(s) - i < 2 {
			return utf8.RuneError
		}
		return (rune(s[i] & 0x1f) << 6) | rune(s[i+1] & 0x3f)
	} else if (s[i] & 0xf0) == 0xe0 {
		if len(s) - i < 3 {
			return utf8.RuneError
		}
		return (rune(s[i] & 0x0f) << 12) | (rune(s[i+1] & 0x3f) << 6) | rune(s[i+2] & 0x3f)
	} else if (s[i] & 0xf8) == 0xf0 {
		if len(s) - i < 3 {
			return utf8.RuneError
		}
		return (rune(s[i] & 0x07) << 18) | (rune(s[i+1] & 0x3f) << 12) | (rune(s[i+2] & 0x3f) << 6) | rune(s[i+2] & 0x3f)
	} else {
		return utf8.RuneError
	}
}

func Choose(source *engine.Identifier) string {
	if source.Parameter == nil {
		return "No parameters specified"
	}

	args := strings.Split(*source.Parameter, " ")
	j := 0
	for i := range args {
		// Remove all empty strings from args, which occurs when multiple spaces seperate values.
		args[j] = args[i]
		if args[i] != "" {
			j++
		}
	}
	args = args[:j]

	return args[rand.Intn(len(args))]
}

func Whisper(source *engine.Identifier) string {
	if source.Parameter == nil {
		return "ʷʰᵃᵗ ʷᵒᵘˡᵈ ʸᵒᵘ ˡⁱᵏᵉ ᵐᵉ ᵗᵒ ˢᵃʸ"
	}

	var out []rune = make([]rune, len(*source.Parameter))
	for i := range *source.Parameter {
		if (*source.Parameter)[i] >= '0' && (*source.Parameter)[i] <= '9' {
			out[i] = lowerNumbers[(*source.Parameter)[i] - '0']
		} else if (*source.Parameter)[i] >= 'a' && (*source.Parameter)[i] <= 'z' {
			out[i] = lowerLetters[(*source.Parameter)[i] - 'a']
		} else if (*source.Parameter)[i] >= 'A' && (*source.Parameter)[i] <= 'Z' {
			out[i] = lowerLetters[(*source.Parameter)[i] - 'A']
		} else {
			out[i] = toRune(*source.Parameter, i)
		}
	}

	return string(out)
}

func Scream(source *engine.Identifier) string {
	if source.Parameter == nil {
		return "Ｗｈａｔ  ｄｏ  ｙｏｕ  ｗａｎｔ  ｍｅ  ｔｏ  ｓａｙ？"
	}

	var out []byte = make([]byte, 0, len(*source.Parameter))
	for i := range *source.Parameter {
		if (*source.Parameter)[i] == ' ' {
			out = append(out, ' ', ' ')
		} else if (*source.Parameter)[i] > 0x20 && (*source.Parameter)[i] < 0x7f {
			out = append(out, 0x0, 0x0, 0x0)
			utf8.EncodeRune(out[len(out)-3:], 0xff00 + rune((*source.Parameter)[i]) - 0x20)
		} else {
			out = append(out, (*source.Parameter)[i])
		}
	}

	return string(out)
}

func Nice(source *engine.Identifier) string {
	if source.Parameter == nil {
		return "𝓦𝓱𝓪𝓽 𝔀𝓸𝓾𝓵𝓭 𝔂𝓸𝓾 𝓵𝓲𝓴𝓮 𝓶𝓮 𝓽𝓸 𝓼𝓪𝔂?"
	}

	var out []byte = make([]byte, 0, len(*source.Parameter))
	for i := range *source.Parameter {
		if (*source.Parameter)[i] >= 'A' && (*source.Parameter)[i] <= 'Z' {
			out = append(out, 0x0, 0x0, 0x0, 0x0)
			utf8.EncodeRune(out[len(out)-4:], 0x1d4d0 + rune((*source.Parameter)[i]) - rune('A'))
		} else if (*source.Parameter)[i] >= 'a' && (*source.Parameter)[i] <= 'z' {
			out = append(out, 0x0, 0x0, 0x0, 0x0)
			utf8.EncodeRune(out[len(out)-4:], 0x1d4ea + rune((*source.Parameter)[i]) - rune('a'))
		} else {
			out = append(out, (*source.Parameter)[i])
		}
	}

	return string(out)
}

func Is(source *engine.Identifier) string {
	return answers[rand.Intn(len(answers))]
}

func Caesar(source *engine.Identifier) string {
	if source.Parameter == nil {
		return "No parameter specified"
	}

	split := strings.SplitN(*source.Parameter, " ", 2)
	if len(split) == 1 {
		return "No text specified"
	}

	shift, err := strconv.Atoi(split[0])
	if err != nil {
		return "Shift parameter is not a number"
	}

	var cipher = make([]byte, len(split[1]))
	for i := range split[1] {
		if split[1][i] >= 'a' && split[1][i] <= 'z' {
			cipher[i] = ((split[1][i] - 'a' + byte(shift)) % 26) + 'a'
		} else if split[1][i] >= 'A' && split[1][i] <= 'Z' {
			cipher[i] = ((split[1][i] - 'A' + byte(shift)) % 26) + 'A'
		} else {
			cipher[i] = split[1][i]
		}
	}

	return string(cipher)
}

func Give(source *engine.Identifier) string {
	if source.Parameter == nil {
		return "No parameter specified"
	}

	split := strings.SplitN(*source.Parameter, " ", 2)
	if len(split) == 1 {
		return "No command specified"
	}

	param := strings.SplitN(split[1], " ", 2)
	command, ok := engine.Methods[param[0]]
	if !ok {
		return "No such method"
	}

	nick := engine.Nickname{split[0], engine.ModeNone, false}
	source.Nick = &nick
	source.Command = param[0]
	if len(split) == 1 {
		source.Parameter = nil
	} else {
		source.Parameter = &param[1]
	}

	return command.Func(source)
}
