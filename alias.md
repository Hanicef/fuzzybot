
# Alias documentation

## Terms

* Alias - The name of the alias, as specified as the first argument in `.addalias`.
* Expansion - What the alias will result into, as specified as the second argument in `.addalias`.
* Sequence - Expansion "rule"; will be replaced with other text depending on what sequence is specified.
* Condition - The sequences `%i`, `%t`, optionally `%e`, and `%x` in the respective order.
* Invocation - When an alias is commanded to be displayed by the bot, it is being invoked.
* Caller - Person invoking the alias.

## Invocation

An alias can be invoked with `!alias`. If the alias doesn't exist, the bot will
respond with either of these four messages:

* `( ͠° ͟ʖ ͡°)`
* `ಠ_ಠ`
* `ヽ(。_°)ノ`
* `¯\(◉◡◔)/¯`

If an invocation is prepended with `@nickname`, a 'cloaked' invocation will
occur, where a user can then invoke an alias with a different nickname.

## Sequences

### `%%`: Percent

Becomes a single percent sign. Used to allow writing percent signs without
conflicting with a different sequence.

### `%u`: Caller nickname

Expands to the nickname of the caller.

### `%n`: Argument

Expands to the argument *n*, as specified when the alias is invoked. Will be
empty if no argument of that level is specified during invocation.

### `%*`: All arguments

Expands to all arguments, seperating each argument with a space.

### `%i`...`%t`...`%e`...`%x`: Conditional statement

Builds a conditional statement. Either `%t`...`%e` or `%e`...`%x` will be
expanded based on if the condition between `%i`...`%t` is true or false,
respectively.

The condition (`%i`...`%t`) must have either:

* An equality (`=`); or
* An inequality (`/=`)

If an equality is invoked, the left and the right text must match for the
condition to be true. If an inequality is invoked, the left and the right text
must not match for the condition to be true.

### `%{`...`}`: Command

Expands to the text that would normally be replied to from a method. Has the
same syntax as a command, with the exception being that the dot is absent.

