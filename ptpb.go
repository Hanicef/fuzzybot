package main

import (
	"net/http"
	"net/url"
	"encoding/json"
	"bytes"
	"fmt"
	"errors"
	"time"
)

type newPaste struct {
	Content string `json:"content"`
	Filename string `json:"filename"`
	Sunset string `json:"sunset"`
}

type newPasteResponse struct {
	Url string `json:"url"`
	Sunset string `json:"sunset"`
}

// Type hack to be able to put a strings.Reader into http.Request.Body
type ByteReadCloser struct {
	stream *bytes.Reader
}

func (s ByteReadCloser) Read(p []byte) (n int, err error) {
	return s.stream.Read(p)
}

func (s ByteReadCloser) Close() error {
	// Do nothing; a string stream cannot be closed anyway.
	return nil
}

func Fetch(method, weburl, contentType string, body []byte) (*http.Response, error) {
	var client = http.Client{
		nil, nil, nil,
		time.Second * 15,
	}

	url, err := url.Parse(weburl)
	if err != nil {
		return nil, err
	}

	req := http.Request{
		Method: method,
		URL: url,
		Header: make(http.Header, 2),
		ContentLength: int64(len(body)),
		TransferEncoding: nil,
		Close: true,
		Host: "",
	}

	if body == nil {
		req.Body = nil
	} else {
		req.Body = ByteReadCloser{bytes.NewReader(body)}
	}

	req.Header["User-Agent"] = []string{"fuzzybot/1.0"}
	if contentType != "" {
		req.Header["Content-Type"] = []string{contentType + "; charset=utf-8"}
		req.Header["Accept"] = []string{contentType}
	} else {
		req.Header["Accept"] = []string{"*"}
	}

	res, err := client.Do(&req)
	return res, err
}

func createPaste(name, data string) (string, string, error) {
	form := newPaste{data, name, time.Now().Add(time.Hour * 24).Format(time.RFC3339)}
	body, err := json.Marshal(form)
	if err != nil {
		return "", "", err
	}

	resp, err := Fetch("POST", "https://ptpb.pw", "application/json", body)
	if err != nil {
		return "", "", fmt.Errorf("Fetch(): %s", err.Error())
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return "", "", errors.New("body is empty")
	}

	out := newPasteResponse{}
	err = json.NewDecoder(resp.Body).Decode(&out)
	if err != nil {
		return "", "", fmt.Errorf("Decode(): %s", err.Error())
	}

	return out.Url, out.Sunset, nil
}
