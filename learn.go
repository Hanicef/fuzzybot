package main

import (
	"gitlab.com/Hanicef/fuzzybot/engine"
	"regexp"
	"strings"
	"database/sql"
	"log"
	"io"
	"bufio"
	"fmt"
	"bytes"
	"strconv"
	"unicode/utf8"
)

var emailRegex = regexp.MustCompile(`\w+@(?:\w+\.)+\w+`)
var ipv4Regex = regexp.MustCompile(`(?:\d+\.){3}\d+`)
var ipv6Regex = regexp.MustCompile(`(?:[\da-fA-F]+:)+[\da-fA-F]+|[\da-fA-F:]*::[\da-fA-F:]*`)

var learnLock bool = false

func escapeHTML(data []byte) string {
	var out = make([]byte, 0, len(data))
	var seq []byte

	for i := 0; i < len(data); i++ {
		if data[i] == '&' {
			j := i+1
			for ; j < len(data) && data[j] != ';'; j++ { }
			if j == len(data) {
				return ""
			}

			if i+1 == j {
				// Empty escape
				return ""
			}

			seq = data[i+1:j]
			if seq[0] == '#' {
				var c int64
				var err error

				if len(seq) > 1 && seq[1] == 'x' {
					if len(seq) == 2 {
						return ""
					}

					c, err = strconv.ParseInt(string(seq[2:]), 16, 32)
				} else {
					if len(seq) == 1 {
						return ""
					}

					c, err = strconv.ParseInt(string(seq[1:]), 10, 32)
				}

				if err != nil {
					return ""
				}

				rl := utf8.RuneLen(rune(c))
				switch rl {
				case 4:
					out = append(out, ' ')
					fallthrough
				case 3:
					out = append(out, ' ')
					fallthrough
				case 2:
					out = append(out, ' ')
					fallthrough
				case 1:
					out = append(out, ' ')
				}
				utf8.EncodeRune(out[len(out)-rl:], rune(c))
			} else {
				switch string(seq) {
				case "quot":
					out = append(out, '"')
				case "amp":
					out = append(out, '&')
				case "apos":
					out = append(out, '\'')
				case "lt":
					out = append(out, '<')
				case "gt":
					out = append(out, '>')
				default:
					return ""
				}
			}

			i = j
		} else {
			out = append(out, data[i])
		}
	}

	return string(out)
}

func learn(line string) {
	var row *sql.Row
	var res sql.Result
	var err error
	var value int64
	var last int64
	var rcount int64

	engine.LockDatabase()
	defer engine.UnlockDatabase()

	last = 0
	for _, w := range strings.Split(line, " ") {
		if w == "" {
			// Skip double spaces
			continue
		}

		if urlRegex.MatchString(w) || emailRegex.MatchString(w) || ipv4Regex.MatchString(w) || ipv6Regex.MatchString(w) {
			continue
		}

		res, err = engine.Database.Exec("UPDATE Word SET occur = occur + 1 WHERE word = $1", w)
		if err != nil {
			log.Print("Markov error: ", err.Error())
		}

		rcount, err = res.RowsAffected()
		if err != nil {
			log.Print("Markov error: ", err.Error())
		} else if rcount == 0 {
			res, err = engine.Database.Exec("INSERT INTO Word(word, occur) VALUES ($1, 1)", w)
			if err != nil {
				log.Print("Markov error: ", err.Error())
			}
		}

		row = engine.Database.QueryRow("SELECT id FROM Word WHERE word = $1", w)
		err = row.Scan(&value)
		//value, err = res.LastInsertId()
		if err != nil {
			log.Print("Markov error: ", err.Error())
		}

		if last != 0 {
			// Save word sequence.
			res, err = engine.Database.Exec("UPDATE WordConnection SET occur = occur + 1 WHERE prev = $1 AND next = $2", last, value)
			if err != nil {
				log.Print("Markov error: ", err.Error())
			}

			rcount, err = res.RowsAffected()
			if err != nil {
				log.Print("Markov error: ", err.Error())
			} else if rcount == 0 {
				_, err = engine.Database.Exec("INSERT INTO WordConnection(prev, next, occur) VALUES ($1, $2, 1)", last, value)
				if err != nil {
					log.Print("Markov error: ", err.Error())
				}
			}
		} else {
			// Save starting word.
			res, err = engine.Database.Exec("UPDATE StartWord SET occur = occur + 1 WHERE word = $1", value)
			if err != nil {
				log.Print("Markov error: ", err.Error())
			}

			rcount, err = res.RowsAffected()
			if err != nil {
				log.Print("Markov error: ", err.Error())
			} else if rcount == 0 {
				_, err = engine.Database.Exec("INSERT INTO StartWord(word, occur) VALUES ($1, 1)", value)
				if err != nil {
					log.Print("Markov error: ", err.Error())
				}
			}
		}

		last = value
	}
}

func Learn(source *engine.Identifier, matches []string) string {
	if learnLock {
		// Ignore chat while learning from URL
		return ""
	}

	if source.Command[0] == '.' || source.Command[0] == '!' || source.Command[0] < ' ' {
		// If the text is a command or binary, don't save it!
		return ""
	}

	learn(source.Command)
	return ""
}

func learnPlain(data io.Reader) error {
	var line string
	var err error
	var stream = bufio.NewReader(data)

	for {
		// Read the data in lines.
		line, err = stream.ReadString('\n')
		if err != nil {
			if err == io.EOF {
				return nil
			} else {
				return err
			}
		}

		// Don't process empty lines.
		line = strings.TrimSpace(line)
		if line != "" {
			learn(line)
		}
	}
}

func learnHTML(data io.Reader) (bool, error) {
	var tag []byte
	var para []byte
	var inpara bool
	var char byte
	var err error
	var stream = bufio.NewReader(data)

	for {
		char, err = stream.ReadByte()
		if err != nil {
			if err == io.EOF {
				// If para is nil, no paragraphs could be found.
				return para != nil, nil
			} else {
				return false, err
			}
		}

		if char != '<' && inpara {
			// If we're in a paragraph, save the text for learning.
			para = append(para, char)
		}

		if char == '<' {
			tag, err = parseTag(stream)
			if err != nil {
				return false, err
			}

			tag = extractTag(tag)
			if !inpara && bytes.Compare(tag, []byte{'p'}) == 0 {
				inpara = true
			} else if inpara && bytes.Compare(tag, []byte{'/', 'p'}) == 0 {
				inpara = false
				text := escapeHTML(para)
				if text != "" {
					// Learn only if it's escaped properly.
					learn(text)
				}

				para = para[:0]
			}
		}
	}
}

func LearnHTTP(source *engine.Identifier) string {
	if source.Parameter == nil {
		return "No parameter specified"
	}

	// To make sure the bot isn't DOS-attacked through the learning routines, only allow one thread to use the routines at a time.
	if learnLock {
		return "Learning routines are currently occupied; try again later"
	}

	if	!urlRegex.MatchString(*source.Parameter) {
		return "Only HTTP URLs can be processed"
	}

	resp, err := Fetch("GET", *source.Parameter, "", nil)
	if err != nil {
		return fmt.Sprint("Failed to send GET request: ", err.Error())
	}
	defer resp.Body.Close()

	learnLock = true
	defer func () { learnLock = false }()

	contentType := resp.Header.Get("Content-Type")
	content := strings.Split(contentType, "; ")
	switch strings.ToLower(content[0]) {
	case "text/plain":
		err := learnPlain(resp.Body)
		if err != nil {
			return fmt.Sprint("Failed to learn: ", err.Error())
		}
	case "text/html":
		status, err := learnHTML(resp.Body)
		if err != nil {
			return fmt.Sprint("Failed to learn: ", err.Error())
		}

		if !status {
			return "No paragraphs found in HTML"
		}
	default:
		return fmt.Sprint("Can't probe content type of ", content[0])
	}

	return "Done"
}
