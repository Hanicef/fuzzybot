
package main

import (
	"gitlab.com/Hanicef/fuzzybot/engine"
	"fmt"
	"strings"
	"bytes"
	"encoding/json"
)

func searchMediaWiki(url string, search string) ([][]string, error) {
	var data []interface{}
	var out [][]string

	search = strings.Replace(search, " ", "%20", -1)
	resp, err := Fetch("GET", url + "/w/api.php?action=opensearch&format=json&formatversion=2&search=" + search, "", nil)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return nil, err
	}

	// TODO: Check interfaces.
	out = make([][]string, 3)
	for j := 0; j < 3; j++ {
		out[j] = make([]string, len(data[j+1].([]interface{})))
		for i, face := range data[j+1].([]interface{}) {
			out[j][i] = face.(string)
		}
	}
	return out, nil
}

func SearchWikipedia(source *engine.Identifier) string {
	if source.Parameter == nil {
		return "No parameter specified"
	}
	data, err := searchMediaWiki("https://en.wikipedia.org", *source.Parameter)
	if err != nil {
		return fmt.Sprint("Could not search https://en.wikipedia.org/: ", err.Error())
	}

	if len(data[0]) == 0 {
		return "No results"
	} else {
		return fmt.Sprintf("%s: %s", data[0][0], data[2][0])
	}
}

func scrapeWiktionary(url string) ([]byte, error) {
	var b [1]byte
	var tag []byte
	var err error
	var data = make([]byte, 0, 8)
	var depth int = 0

	resp, err := Fetch("GET", url, "", nil)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	for {
		_, err = resp.Body.Read(b[:])
		if err != nil {
			return nil, err
		}

		if b[0] == '<' {
			tag, err = parseTag(resp.Body)
			if err != nil {
				return nil, err
			}

			// Search for an ordered list; those are the definitions.
			if bytes.Compare(extractTag(tag), []byte{'o','l'}) == 0 {
				for {
					_, err = resp.Body.Read(b[:])
					if err != nil {
						return nil, err
					}

					if b[0] == '<' {
						tag, err = parseTag(resp.Body)
						if err != nil {
							return nil, err
						}

						if bytes.Compare(extractTag(tag), []byte{'l','i'}) == 0 {
							for {
								_, err = resp.Body.Read(b[:])
								if err != nil {
									return nil, err
								}

								if b[0] == '\n' {
									// Safe to assume an end on EOL
									return data, nil
								} else if b[0] == '<' {
									tag, err = parseTag(resp.Body)
									if err != nil {
										return nil, err
									}

									if depth <= 0 && bytes.Compare(extractTag(tag), []byte{'/','l','i'}) == 0 {
										return data, nil
									} else if bytes.Compare(extractTag(tag), []byte{'o','l'}) == 0 {
										// Lists can have sublists; keep track of this.
										depth++
									} else if bytes.Compare(extractTag(tag), []byte{'/','o','l'}) == 0 {
										depth--
									}
								} else {
									data = append(data, b[0])
								}
							}
						}
					}
				}
			}
		}
	}
}

func SearchWiktionary(source *engine.Identifier) string {
	if source.Parameter == nil {
		return "No parameter specified"
	}
	data, err := searchMediaWiki("https://en.wiktionary.org", *source.Parameter)
	if err != nil {
		return fmt.Sprint("Could not search https://en.wiktionary.org/: ", err.Error())
	}

	if len(data[0]) == 0 {
		return "No results"
	} else {
		def, err := scrapeWiktionary(data[2][0])
		if err != nil {
			return fmt.Sprint("Failed to scrape definition: ", err.Error())
		} else {
			return fmt.Sprintf("%s [%s]: %s", data[0][0], data[2][0], string(def))
		}
	}
}
