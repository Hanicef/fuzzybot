
package main

import (
	"gitlab.com/Hanicef/fuzzybot/engine"
	"encoding/json"
	"strings"
	"fmt"
)

type DDGResult struct {
	Result string
	FirstURL string
	Icon struct {
		URL string
		Width string
		Height string
	}
	Text string
}

type DDGResponse struct {
	Abstract string
	AbstractText string
	AbstractSource string
	AbstractURL string
	Image string
	Heading string

	Answer string
	AnswerType string

	Definition string
	DefinitionSource string
	DefinitionURL string

	RelatedTopics []DDGResult
	Results []DDGResult

	Type string
	Redirect string
}

func Search(source *engine.Identifier) string {
	if source.Parameter == nil {
		return "No parameter specified."
	}

	search := strings.Replace(*source.Parameter, " ", "%20", -1)
	resp, err := Fetch("GET", "https://api.duckduckgo.com?format=json&skip_disambig=1&q=" + search, "", nil)
	if err != nil {
		return fmt.Sprint("Could not GET to https://duckduckgo.com/: ", err.Error())
	}
	defer resp.Body.Close()

	var response DDGResponse
	err = json.NewDecoder(resp.Body).Decode(&response)
	if err != nil {
		return fmt.Sprint("Could not decode https://duckduckgo.com/: ", err.Error())
	}

	return response.AbstractURL
}
