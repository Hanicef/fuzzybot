package main

import (
	"testing"
	"strings"
)

func matchUrl(context *testing.T, url string) {
	if !urlRegex.MatchString(url) {
		context.Error(url, "- Failed match")
	}
}

func TestUrlMatch(context *testing.T) {
	matchUrl(context, "http://google.com/")
	matchUrl(context, "https://127.0.0.1/file.txt")
	matchUrl(context, "https://[::1]/file.txt")
	matchUrl(context, "https://nice.website.com?search=foo")
	matchUrl(context, "https://nice.website.com#someFragment")
	matchUrl(context, "https://foo.bar/baz#at")
}

func testTitle(context *testing.T, test, expect string) {
	stream := strings.NewReader(test)
	title := findTitle(stream)
	if title != expect {
		if expect == "" {
			context.Error(test, "is not empty")
		} else {
			context.Error(title, "!=", expect)
		}
	}
}

func TestFindTitle(context *testing.T) {
	testTitle(context, "<head><title>foo</title></head>", "foo")
	testTitle(context, "<head></head>", "")
	testTitle(context, "<head><title></head>", "")
	testTitle(context, "<head><title>foo</title</head>", "")
	testTitle(context, "<head><title>foo", "")
}
