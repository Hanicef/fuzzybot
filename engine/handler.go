
package engine

import (
	irc "github.com/fluffle/goirc/client"
	"log"
	"time"
)

func handleBan(conn *irc.Conn, line *irc.Line) {
	log.Print("ERROR: Bot is banned from ", line.Args[1], ", giving up.")
	Quit <- true
}

func handleServerBan(conn *irc.Conn, line *irc.Line) {
	log.Print("ERROR: Bot is banned from ", Server, ", giving up.")
	Quit <- true
}

func handleInviteOnly(conn *irc.Conn, line *irc.Line) {
	log.Print("WARNING: Channel ", Channel, " is marked (+i). Awaiting invite...")
}

func handleInvite(conn *irc.Conn, line *irc.Line) {
	if line.Args[1] == Channel {
		conn.Join(Channel)
	}
}

func handleKick(conn *irc.Conn, line *irc.Line) {
	if line.Args[1] == BotNick {
		log.Print("WARNING: Bot was kicked, rejoining...")
		conn.Join(Channel)
	}
}

func handleTooManyChannels(conn *irc.Conn, line *irc.Line) {
	log.Print("WARNING: Bot has reached channel limit. Disconnecting...")
	Quit <- false
}

func handleFull(conn *irc.Conn, line *irc.Line) {
	log.Print("INFO: Channel ", line.Args[1], " is full! Trying again in 15 seconds...")
	time.Sleep(time.Second * 15)
	conn.Join(Channel)
}

func handleOccupiedNick(conn *irc.Conn, line *irc.Line) {
	log.Print("WARNING: Nickname ", BotNick, " is occupied! Disconnecting...")
	conn.Nick(BotNick + "|")
	Quit <- false
}

func handleNotInChannel(conn *irc.Conn, line *irc.Line) {
	log.Print("WARNING: Bot not in channel! Joining...")
	conn.Join(Channel)
}

func handleOpRequired(conn *irc.Conn, line *irc.Line) {
	log.Print("ERROR: Channel ", Channel, " requires channel operator, giving up.")
	Quit <- true
}
