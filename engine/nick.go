
package engine

import (
	irc "github.com/fluffle/goirc/client"
	"log"
	"fmt"
	"strings"
	"time"
)

const (
	ModeNone = iota
	ModeVoice
	ModeHalfOp
	ModeOp
	ModeAdmin
	ModeOwner
)

type Nickname struct {
	Nick string
	Mode int
	Present bool
}

var nicks []Nickname

func FindNick(nick string) *Nickname {
	for i := range nicks {
		if nicks[i].Nick == nick {
			return &nicks[i]
		}
	}

	return nil
}

func ircLower(s string) string {
	// We can't use the built-in lowercase in Go, as the lowercasing is different.
	// See: https://tools.ietf.org/html/rfc1459#section-2.2
	var out []byte = make([]byte, len(s))
	for i := range s {
		// See Unicode for the corresponding hex codes.
		if s[i] > 0x40 && s[i] < 0x5e {
			out[i] = s[i] + 0x20
		} else {
			out[i] = s[i]
		}
	}

	return string(out)
}

func (nick *Nickname) Lower() string {
	return ircLower(nick.Nick)
}

func (nick *Nickname) GetModeRune() rune {
	switch nick.Mode {
	case ModeNone: return ' '
	case ModeVoice: return '+'
	case ModeHalfOp: return '%'
	case ModeOp: return '@'
	case ModeAdmin: return '&'
	case ModeOwner: return '~'
	}
	panic("invalid mode on nickname")
}

func logStdout(format ...interface{}) {
	fmt.Print(append([]interface{}{time.Now().Format(timeFormat), " "}, format...)...)
}

// Since nickname tracking is not automatic, we have to keep track of it ourselves.
func onNickEvent(conn *irc.Conn, line *irc.Line) {
	switch line.Cmd {
	case "353": // RPL_NAMREPLY
		names := strings.Split(line.Text(), " ")
		nicks = make([]Nickname, len(names))
		for i := range names {
			if names[i] == "" {
				continue
			}

			logStdout("NICK [", names[i], "]\n")

			switch names[i][0] {
			case '+':
				nicks[i].Mode = ModeVoice
				names[i] = names[i][1:]
			case '%':
				nicks[i].Mode = ModeHalfOp
				names[i] = names[i][1:]
			case '@':
				nicks[i].Mode = ModeOp
				names[i] = names[i][1:]
			case '&':
				nicks[i].Mode = ModeAdmin
				names[i] = names[i][1:]
			case '~':
				nicks[i].Mode = ModeOwner
				names[i] = names[i][1:]
			}

			nicks[i].Nick = names[i]
			nicks[i].Present = true
		}

	case irc.JOIN:
		logStdout("JOIN [", line.Nick, "] - ", line.Src, "\n")
		nicks = append(nicks, Nickname{ line.Nick, ModeNone, true })

	case irc.PART:
	case irc.QUIT:
		logStdout("PART [", line.Nick, "] - ", line.Src, "\n")
		for i := range nicks {
			if nicks[i].Nick == line.Nick {
				nicks = append(nicks[:i], nicks[i+1:]...)
				return
			}
		}

		// Should not happen, but just in case.
		log.Print("WARNING: got PART from unknown nickname; nick tracking may be out of sync")

	case irc.NICK:
		logStdout("NICK [", line.Nick, "] -> [", line.Args[0], "]\n")
		for i := range nicks {
			if nicks[i].Nick == line.Nick {
				nicks[i].Nick = line.Args[0]
				return
			}
		}

		// Should not happen, but just in case.
		log.Print("WARNING: got NICK from unknown nickname; nick tracking may be out of sync")

	case irc.MODE:
		var nick *Nickname
		var channel = ircLower(line.Args[0])

		if channel != ircLower(BotNick) && channel != ircLower(Channel) {
			// Wrong channel!
			log.Print("WARNING: Bot ended up in wrong channel! Parting and rejoining...")
			conn.Part(channel)
			conn.Join(Channel)
			return
		}

		if len(line.Args) > 2 {
			if line.Nick == "" {
				logStdout("MODE ", line.Args[1], " : ", line.Args[2], "\n")
			} else {
				logStdout("MODE [", line.Nick, "] ", line.Args[1], " : ", line.Args[2], "\n")
			}
		} else {
			if line.Nick == "" {
				logStdout("MODE ", line.Args[1], "\n")
			} else {
				logStdout("MODE [", line.Nick, "] ", line.Args[1], "\n")
			}
		}

		// The only relevant modes we are paying attention to is privileges.
		for _, name := range strings.Split(line.Args[2], " ") {
			nick = FindNick(name)
			if nick == nil {
				// Should not happen, but just in case.
				log.Print("WARNING: got MODE from unknown nickname; nick tracking may be out of sync")
			} else {
				switch line.Args[1] {
				case "+v":
					nick.Mode = ModeVoice
				case "+h":
					nick.Mode = ModeHalfOp
				case "+o":
					nick.Mode = ModeOp
				case "+a":
					nick.Mode = ModeAdmin
				case "+q":
					nick.Mode = ModeOwner
				case "-v", "-h", "-o", "-a", "-q":
					nick.Mode = ModeNone
				}
			}
		}
	}
}

