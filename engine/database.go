
package engine

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"log"
	"sync"
)

var Database *sql.DB
var mutex = &sync.Mutex{}

func initDB() {
	var err error
	Database, err = sql.Open("sqlite3", DatabaseFile)
	if err != nil {
		log.Fatal("ERROR: ", err.Error())
	}

	// Databases can still fail despite opening successfully. Ping the database to make sure it's OK.
	err = Database.Ping()
	if err != nil {
		log.Fatal("ERROR: ", err.Error())
	}

	_, err = Database.Exec("CREATE TABLE IF NOT EXISTS Nick(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nick STRING NOT NULL, lower STRING NOT NULL UNIQUE)")
	if err != nil {
		log.Fatal("ERROR: ", err.Error())
	}
}

func RegisterNick(nick string) (int64, error) {
	var id int64
	nickname := Nickname{nick, ModeNone, false}
	row := Database.QueryRow("SELECT id FROM Nick WHERE lower = $1", nickname.Lower())
	err := row.Scan(&id)
	if err != nil {
		if err == sql.ErrNoRows {
			// This is expected
			res, err := Database.Exec("INSERT INTO Nick(nick, lower) VALUES ($1, $2)", nick, nickname.Lower())
			if err != nil {
				return 0, err
			}

			id, err = res.LastInsertId()
			return id, err
		}
		return 0, err
	}

	return id, nil
}

func GetNick(nick string) (int64, error) {
	var id int64
	nickname := Nickname{nick, ModeNone, false}
	row := Database.QueryRow("SELECT id FROM Nick WHERE lower = $1", nickname.Lower())
	err := row.Scan(&id)
	if err != nil {
		return 0, err
	}

	return id, nil
}

func LockDatabase() {
	// As the database is SQLite, we can't have multiple threads using the database simultaneously.
	// Therefore, the database can be locked with mutex to prevent that.
	mutex.Lock()
}

func UnlockDatabase() {
	mutex.Unlock()
}
