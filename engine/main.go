
package engine

import (
	irc "github.com/fluffle/goirc/client"
	"github.com/go-ini/ini"
	"log"
	"time"
	"flag"
	"strconv"
	"os"
	"os/signal"
)

type KeyVariable struct {
	Name string
	Value interface{}
}

var Client *irc.Conn
var ChatLog string
var LogSizeLimit int64 = 0

var (
	BotNick string = "FuzzyBot"
	UserName string = "fuzzybot"
	IRCName string = "FuzzyBot, by Hanicef"
	Server string
	Channel string
	UseSSL bool = false

	UseNickServ bool = false
	Password string

	DatabaseFile string
)

var Quit chan bool = make(chan bool, 1)

func LoadConfig(sections map[string][]KeyVariable) {
	if sections == nil {
		sections = make(map[string][]KeyVariable, 1)
	}

	// Prepare for variable mapping.
	sections["connection"] = []KeyVariable {
		KeyVariable{"nick", &BotNick},
		KeyVariable{"user", &UserName},
		KeyVariable{"name", &IRCName},
		KeyVariable{"server", &Server},
		KeyVariable{"channel", &Channel},
		KeyVariable{"use_ssl", &UseSSL},
		KeyVariable{"use_nickserv", &UseNickServ},
		KeyVariable{"password", &Password},
	}

	sections["bot"] = []KeyVariable {
		KeyVariable{"prefix", &CommandPrefix},
		KeyVariable{"invalid_respond", &InvalidRespond},
		KeyVariable{"chat_log", &ChatLog},
		KeyVariable{"log_max_size", &LogSizeLimit},
	}

	file := flag.String("config", "~/.fuzzybot/config.cfg", "Configuration file for FuzzyBot")
	dbfile := flag.String("database", "~/.fuzzybot/database.db", "Database file for FuzzyBot")
	flag.Parse()
	DatabaseFile = *dbfile

	config, err := ini.InsensitiveLoad(*file)
	if err != nil {
		log.Fatal("ERROR: ", err.Error())
	}

	section, err := config.GetSection("connection")
	if err != nil {
		log.Fatal("ERROR: ", err.Error())
	}

	for k, v := range sections {
		section, err = config.GetSection(k)
		if err != nil {
			// Ignore the entire section if it's not found.
			continue
		}

		for i := range v {
			key, err := section.GetKey(v[i].Name)
			if err != nil {
				continue
			}

			str, ok := v[i].Value.(*string)
			if ok {
				// As IRC expects # for a channel, ignore comments if we want a string.
				*str = key.String() + key.Comment
				continue
			}

			in, ok := v[i].Value.(*int)
			if ok {
				*in, err = key.Int()
				if err != nil {
					log.Fatal("ERROR: ", err.Error())
				}
				continue
			}

			i64, ok := v[i].Value.(*int64)
			if ok {
				*i64, err = key.Int64()
				if err != nil {
					log.Fatal("ERROR: ", err.Error())
				}
				continue
			}

			c, ok := v[i].Value.(*byte)
			if ok {
				*c = key.String()[0]
				continue
			}

			b, ok := v[i].Value.(*bool)
			if ok {
				*b, err = key.Bool()
				if err != nil {
					log.Fatal("ERROR: ", err.Error())
				}
				continue
			}

			panic("section type not handled")
		}
	}

	initDB()
}

func redirectChatLog(file string) {
	stream, err := os.OpenFile(file, os.O_WRONLY | os.O_CREATE | os.O_APPEND, 0644)
	if err != nil {
		log.Fatal("ERROR: ", err.Error())
	}

	stat, err := stream.Stat()
	if err != nil {
		log.Fatal("ERROR: ", err.Error())
	}

	if (LogSizeLimit > 0 && stat.Size() > (LogSizeLimit << 10)) {
		stream.Close()

		// Split logs if they exceed a size limit; useful for handling large log files
		var i int
		for i = 0; ; i++ {
			_, err = os.Stat(file + "." + strconv.Itoa(i))
			if err != nil {
				if os.IsNotExist(err) {
					break
				} else {
					log.Fatal("ERROR: ", err.Error())
				}
			}
		}

		err = os.Rename(file, file + "." + strconv.Itoa(i))
		if err != nil {
			log.Fatal("ERROR: ", err.Error())
		}

		stream, err = os.OpenFile(file, os.O_WRONLY | os.O_CREATE | os.O_APPEND, 0644)
		if err != nil {
			log.Fatal("ERROR: ", err.Error())
		}
	}

	os.Stdout.Close()
	os.Stdout = stream
}

func Start() {
	var err error

	config := irc.NewConfig(BotNick, UserName, IRCName)
	config.SSL = UseSSL
	config.Server = Server
	config.Version = "ᕕ( ᐛ )ᕗ iot master race ᕕ( ᐛ )ᕗ"
	Client = irc.Client(config)

	// Set up stdout for logging.
	if ChatLog != "" {
		redirectChatLog(ChatLog)
	}

	Client.HandleFunc(irc.CONNECTED, func (conn *irc.Conn, line *irc.Line) {
		me := conn.Me()
		BotNick = me.Nick
		UserName = me.Ident

		if UseNickServ {
			conn.Privmsg("NickServ", "IDENTIFY " + Password)

			// Wait for NickServ identification to finish before joining
			time.Sleep(time.Second / 2)
		}
		conn.Mode(BotNick, "+B")
		conn.Join(Channel)
	})

	disconnect := make(chan bool, 1)
	Client.HandleFunc(irc.DISCONNECTED, func (conn *irc.Conn, line *irc.Line) {
		Quit <- false
		disconnect <- true
	})

	Client.HandleFunc("353", onNickEvent)
	Client.HandleFunc(irc.JOIN, onNickEvent)
	Client.HandleFunc(irc.PART, onNickEvent)
	Client.HandleFunc(irc.NICK, onNickEvent)
	Client.HandleFunc(irc.QUIT, onNickEvent)
	Client.HandleFunc(irc.MODE, onNickEvent)

	Client.HandleFunc(irc.PRIVMSG, onMessage)
	Client.HandleFunc(irc.ACTION, onAction)

	Client.HandleFunc("474", handleBan)
	Client.HandleFunc("471", handleFull)
	Client.HandleFunc("405", handleTooManyChannels)
	Client.HandleFunc(irc.KICK, handleKick)
	Client.HandleFunc("433", handleOccupiedNick)
	Client.HandleFunc("436", handleOccupiedNick)
	Client.HandleFunc("442", handleNotInChannel)
	Client.HandleFunc("473", handleInviteOnly)
	Client.HandleFunc(irc.INVITE, handleInvite)

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt)
	go func() {
		// Listen to interrupts
		<-sig
		Quit <- true
	}()

	err = Client.Connect()
	if err != nil {
		log.Fatal("ERROR: ", err.Error())
	}

	// Wait for exit
	<-Quit
	Client.Quit("Disconnected")
	<-disconnect
}
