
package engine

import (
	irc "github.com/fluffle/goirc/client"
	"log"
	"strings"
	"regexp"
	"math/rand"
	"fmt"
	"time"
)

const timeFormat = "2006-01-02 15:04"

type Identifier struct {
	Nick *Nickname
	User string
	Host string
	Source string

	Command string
	Parameter *string
	Private bool
}

var invalidMethod []string = []string{
	"( ͠° ͟ʖ ͡°)",
	"ಠ_ಠ",
	"ヽ(。_°)ノ",
	"¯\\(◉◡◔)/¯",
}

type Method struct {
	Func func(*Identifier) string
	Desc string
}

type Hook struct {
	Regex *regexp.Regexp
	Func func(*Identifier, []string) string
}

var (
	CommandPrefix byte = '.'
	InvalidRespond bool = true
)

var Methods map[string]Method = make(map[string]Method, 1)
var Hooks []Hook

func RegisterMethod(name string, function func(*Identifier) string, desc string) {
	Methods[name] = Method{function, desc}
}

func RegisterHook(regex *regexp.Regexp, function func(*Identifier, []string) string) {
	Hooks = append(Hooks, Hook{ regex, function })
}

func logMessage(channel string, nick *Nickname, message string) {
	if channel[0] == '#' {
		if strings.HasPrefix(message, "\001ACTION ") {
			fmt.Printf("%s * %s %s\n", time.Now().Format(timeFormat), nick.Nick, message[8:len(message)-1])
		} else {
			fmt.Printf("%s <%c%s> %s\n", time.Now().Format(timeFormat), nick.GetModeRune(), nick.Nick, message)
		}
	} else {
		if strings.HasPrefix(message, "\001ACTION ") {
			fmt.Printf("%s # %s %s\n", time.Now().Format(timeFormat), nick.Nick, message[8:len(message)-1])
		} else {
			fmt.Printf("%s [%c%s] %s\n", time.Now().Format(timeFormat), nick.GetModeRune(), nick.Nick, message)
		}
	}
}

func (i *Identifier) say(msg string) {
	if len(msg) > 0 {
		Client.Privmsg(i.Source, msg)
		logMessage(i.Source, &Nickname{BotNick, ModeNone, true}, msg)
	}
}

func (i *Identifier) reply(msg string) {
	if len(msg) > 0 {
		Client.Privmsgf(i.Source, "%s: %s", i.Nick.Nick, msg)
		logMessage(i.Source, &Nickname{BotNick, ModeNone, true}, i.Nick.Nick + ": " + msg)
	}
}

func onMessage(conn *irc.Conn, line *irc.Line) {
	var invoker Identifier

	if line.Args[0][0] == '#' && ircLower(line.Args[0]) != ircLower(Channel) {
		// Wrong channel!
		log.Print("WARNING: Bot ended up in wrong channel! Parting and rejoining...")
		conn.Part(line.Args[0])
		conn.Join(Channel)
		return
	}

	invoker.Nick = FindNick(line.Nick)
	if invoker.Nick == nil {
		// As the bot allows commands over PM, if the user is not known, create a fake one with no privileges.
		invoker.Nick = &Nickname{line.Nick, ModeNone, false}
	}

	logMessage(line.Args[0], invoker.Nick, line.Text())

	invoker.User = line.Ident
	invoker.Host = line.Host
	if line.Args[0] == BotNick {
		invoker.Source = line.Nick
	} else {
		invoker.Source = line.Args[0]
	}

	message := strings.SplitN(line.Text(), " ", 2)
	if message[0][0] == CommandPrefix {
		invoker.Command = message[0]
		if len(message) > 1 {
			invoker.Parameter = &message[1]
		} else {
			invoker.Parameter = nil
		}
		invoker.Private = !line.Public()

		method, ok := Methods[invoker.Command[1:]]
		if !ok {
			if InvalidRespond {
				invoker.reply(invalidMethod[rand.Intn(len(invalidMethod))])
			}
		} else {
			go func () { invoker.reply(method.Func(&invoker)) }()
		}
	} else {
		invoker.Command = line.Text()
		invoker.Parameter = nil

		for i := range Hooks {
			if Hooks[i].Regex == nil {
				go Hooks[i].Func(&invoker, nil)
			} else {
				matches := Hooks[i].Regex.FindAllStringSubmatch(invoker.Command, -1)
				if matches != nil {
					for j := range matches {
						go invoker.say(Hooks[i].Func(&invoker, matches[j]))
					}
				}
			}
		}
	}
}

func onAction(conn *irc.Conn, line *irc.Line) {
	if line.Args[0][0] == '#' {
		fmt.Printf("%s * %s %s\n", time.Now().Format(timeFormat), line.Nick, line.Text())
	} else {
		fmt.Printf("%s # %s %s\n", time.Now().Format(timeFormat), line.Nick, line.Text())
	}
}
