package main

import (
	"gitlab.com/Hanicef/fuzzybot/engine"
	"strings"
	"strconv"
)

// NOTE: These are utility modules primarily used for aliases, but can be used as ordinary commands.

func Reverse(source *engine.Identifier) string {
	if source.Parameter == nil {
		return ""
	}

	var out = make([]byte, len(*source.Parameter))
	for i := range *source.Parameter {
		out[len(out) - i - 1] = (*source.Parameter)[i]
	}

	return string(out)
}

func Repeat(source *engine.Identifier) string {
	if source.Parameter == nil {
		return ""
	}

	return *source.Parameter + *source.Parameter
}

func Count(source *engine.Identifier) string {
	if source.Parameter == nil {
		return ""
	}

	return strconv.Itoa(len(*source.Parameter))
}

func Head(source *engine.Identifier) string {
	if source.Parameter == nil {
		return ""
	}

	split := strings.SplitN(*source.Parameter, " ", 2)
	i, err := strconv.Atoi(split[0])
	if err != nil {
		// If the tokens before the first space is not a number, return the entire string.
		return *source.Parameter
	}

	if len(split) == 1 {
		return ""
	} else if i <= 0 {
		return ""
	} else if i >= len(split[1]) {
		return split[1]
	}

	return split[1][:i]
}

func CutHead(source *engine.Identifier) string {
	if source.Parameter == nil {
		return ""
	}

	split := strings.SplitN(*source.Parameter, " ", 2)
	i, err := strconv.Atoi(split[0])
	if err != nil {
		// If the tokens before the first space is not a number, return the entire string.
		return *source.Parameter
	}

	if len(split) == 1 {
		return ""
	} else if i <= 0 {
		return split[1]
	} else if i >= len(split[1]) {
		return ""
	}

	return split[1][i:]
}

func Tail(source *engine.Identifier) string {
	if source.Parameter == nil {
		return ""
	}

	split := strings.SplitN(*source.Parameter, " ", 2)
	i, err := strconv.Atoi(split[0])
	if err != nil {
		// If the tokens before the first space is not a number, return the entire string.
		return *source.Parameter
	}

	if len(split) == 1 {
		return ""
	} else if i <= 0 {
		return ""
	} else if i >= len(split[1]) {
		return split[1]
	}

	return split[1][len(split[1])-i:]
}

func CutTail(source *engine.Identifier) string {
	if source.Parameter == nil {
		return ""
	}

	split := strings.SplitN(*source.Parameter, " ", 2)
	i, err := strconv.Atoi(split[0])
	if err != nil {
		// If the tokens before the first space is not a number, return the entire string.
		return *source.Parameter
	}

	if len(split) == 1 {
		return ""
	} else if i <= 0 {
		return split[1]
	} else if i >= len(split[1]) {
		return ""
	}

	return split[1][:len(split[1])-i]
}

