package main

import (
	"testing"
	"gitlab.com/Hanicef/fuzzybot/engine"
)

func assertAliasOk(context *testing.T, expect, got string, err error) {
	if err != nil {
		context.Error("got error:", err.Error());
	} else if got != expect {
		context.Error("got wrong expand:", expect, "!=", got);
	}
}

func assertAliasError(context *testing.T, err error) {
	if err == nil {
		context.Error("no error when expecting one");
	}
}

func TestBasicExpand(context *testing.T) {
	out, err := expand("Hello, world!", "", nil, false)
	assertAliasOk(context, "Hello, world!", out, err)
}

func TestSequenceExpand(context *testing.T) {
	out, err := expand("Hello, %u and %0!", "foo", []string{"bar"}, false)
	assertAliasOk(context, "Hello, foo and bar!", out, err)

	out, err = expand("Hello, %*!", "", []string{"foo", "bar"}, false)
	assertAliasOk(context, "Hello, foo bar!", out, err)

	_, err = expand("%", "", nil, false)
	assertAliasError(context, err)

	_, err = expand("%^ aeiou", "", nil, false)
	assertAliasError(context, err)
}

func TestConditionExpand(context *testing.T) {
	out, err := expand("%i%0=%tNo arguments%x", "", nil, false)
	assertAliasOk(context, "No arguments", out, err)

	out, err = expand("%i%0!=%tNo arguments%eHas arguments%x", "", nil, false)
	assertAliasOk(context, "Has arguments", out, err)

	out, err = expand("%i%0!=%t%i%0=foo%tfoo %xis what I say%x", "", []string{"bar"}, false)
	assertAliasOk(context, "is what I say", out, err)

	_, err = expand("%ifoo%t%x", "", nil, false)
	assertAliasError(context, err)

	_, err = expand("%i=%t", "", nil, false)
	assertAliasError(context, err)
}

func TestCommandExpand(context *testing.T) {
	engine.LockDatabase()
	defer engine.UnlockDatabase()

	engine.RegisterMethod("reverse", Reverse, "")
	out, err := expand("%{reverse hello}", "", nil, false)
	assertAliasOk(context, "olleh", out, err)

	_, err = expand("%{doesntexist}", "", nil, false)
	assertAliasError(context, err)

	_, err = expand("%{", "", nil, false)
	assertAliasError(context, err)

	out, err = expand("%{reverse %{reverse hello}}", "", nil, false)
	assertAliasOk(context, "hello", out, err)
}
